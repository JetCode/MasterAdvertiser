package ir.markazandroid.components.signal;

/**
 * Coded by Ali on 29/07/2017.
 */

public interface SignalReceiver {
    boolean onSignal(Signal signal);
}
