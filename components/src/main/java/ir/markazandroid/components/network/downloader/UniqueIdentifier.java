package ir.markazandroid.components.network.downloader;

/**
 * Coded by Ali on 6/22/2019.
 */
public interface UniqueIdentifier<K> {

    K getId();
}
