package ir.markazandroid.components.network;


import ir.markazandroid.components.model.ErrorObject;

/**
 * Coded by Ali on 03/11/2017.
 */

public interface OnResultLoaded<T> {
    void loaded(T result);

    void failed(Exception e);

    interface ActionListener<T> {
        void onSuccess(T successResult);

        void onError(ErrorObject error);

        void failed(Exception e);
    }

    interface AvailableListener<T> {
        void onAvailable(T successResult);

        void onNotAvailable();

        void failed(Exception e);
    }


}
