package ir.markazandroid.components;

import ir.markazandroid.components.aidl.PoliceBridge;
import ir.markazandroid.components.db.DataBase;
import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.components.network.NetworkClient;
import ir.markazandroid.components.network.downloader.MasterDownloader;
import ir.markazandroid.components.signal.SignalManager;

public interface ComponentContainer {

    DataBase getDataBase();

    Parser getParser();

    NetworkClient getNetworkClient();

    SignalManager getSignalManager();

    MasterDownloader getMasterDownloader();

    boolean isInternetConnected();

    PoliceBridge getPoliceBridge();
}
