package ir.markazandroid.widget.clock;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import ir.markazandroid.components.model.EFile;
import ir.markazandroid.components.network.JSONParser.annotations.JSON;
import ir.markazandroid.widget.R;
import ir.markazandroid.widget.ScrollTextView;
import ir.markazandroid.widget.TransparentWidget;

import static android.content.ContentValues.TAG;

/**
 * Analog Clock view
 *
 * @author sylsau - sylvain.saurel@gmail.com - http://www.ssaurel.com
 */
public class AnalogClock extends TransparentWidget {

    /**
     * center X.
     */
    private float x;
    /**
     * center Y.
     */
    private float y;
    private int radius;
    private Calendar cal;
    private Paint paint;
    private Bitmap clockDial = BitmapFactory.decodeResource(getResources(),
            R.drawable.widgetdial);
    private int sizeScaled = -1;
    private Bitmap clockDialScaled;
    private AnalogClock.Params params;
    /**
     * Hands colors.
     */
    private int[] colors;
    private boolean displayHandSec;
    private Timer timer;

    public AnalogClock(Context context) {
        super(context);
        cal = Calendar.getInstance();
    }

    /**
     * this funcition config ui property of this module base on
     * its json
     *
     * @param extras corresponding json for config class ui property
     */
    @Override
    protected void initWidget(JSONObject extras) {



        Paint paint;
        params = parser.get(AnalogClock.Params.class, extras);

        int col = params.get_text_color_int();
        int[] colors = {col,col, col};

        displayHandSec = true;
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);

        int size = Math.min(getWidth(), getHeight());

        this.config(getWidth() / 2, getHeight() / 2, size,
                new Date(), paint, colors, displayHandSec);
        timer = new Timer();

        TimerTask UpdateView = new AnalogClock.UpdateView();
        timer.scheduleAtFixedRate(UpdateView, 0, 1000);

        if (!isBackgroundSet())
            setBackgroundColor(Color.WHITE);
        else
            setBackgroundColor(Color.TRANSPARENT);


    }
    public static class Params {


        private String textColor ;


        @JSON(name="handleColor")
        public String getTextColor() {
            return textColor;
        }



        public void setTextColor(String textColor)
        {

            this.textColor   = textColor;

        }
        public int get_text_color_int()
        {
            if(textColor == null||textColor.equals(""))
            {


                return Color.argb(255,255,255,100);
            }
            int result = 0;

            result   =  Integer.parseInt( textColor.substring(1,textColor.length()),16);
            result+=Color.BLACK;
            return result;
        }



    }

    class UpdateView extends TimerTask {


        public void run() {
            Log.e(TAG, "run: ");
            cal.setTime(new Date());
            postInvalidate();

            //calculate the new position of myBall
        }
    }

    public void config(float x, float y, int size, Date date, Paint paint, int[] colors, boolean displayHandSec) {
        this.x = x;
        this.y = y;
        this.paint = paint;
        this.colors = colors;
        this.displayHandSec = displayHandSec;

        cal.setTime(date);

        // scale bitmap if needed
        if (size != sizeScaled) {
            clockDialScaled = Bitmap.createScaledBitmap(clockDial, size, size, false);
            radius = size / 2;
        }
    }

    /**
     * this method diallocate al resurces that allocated durng runing process
     */
    @Override
    protected void disposeWidget() {
        timer.cancel();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        //super.onDraw(canvas);

        if (paint != null) {
            // draw clock img
            canvas.drawBitmap(clockDialScaled, x - radius, y - radius, null);

            float sec = cal.get(Calendar.SECOND);
            float min = cal.get(Calendar.MINUTE);
            float hour = cal.get(Calendar.HOUR_OF_DAY);
            hour += min / 60.0f;

            //draw hands
            paint.setColor(colors[0]);
            canvas.drawLine(x, y, (float) (x + (radius * 0.5f) * Math.cos(Math.toRadians((hour / 12.0f * 360.0f) - 90f))),
                    (float) (y + (radius * 0.5f) * Math.sin(Math.toRadians((hour / 12.0f * 360.0f) - 90f))), paint);
            canvas.save();
            paint.setColor(colors[1]);
            canvas.drawLine(x, y, (float) (x + (radius * 0.6f) * Math.cos(Math.toRadians((min / 60.0f * 360.0f) - 90f))),
                    (float) (y + (radius * 0.6f) * Math.sin(Math.toRadians((min / 60.0f * 360.0f) - 90f))), paint);
            canvas.save();

            if (displayHandSec) {
                paint.setColor(colors[2]);
                canvas.drawLine(x, y, (float) (x + (radius * 0.7f) * Math.cos(Math.toRadians((sec / 60.0f * 360.0f) - 90f))),
                        (float) (y + (radius * 0.7f) * Math.sin(Math.toRadians((sec / 60.0f * 360.0f) - 90f))), paint);
            }
        }
    }

    protected void onDraw(Canvas canvas) {

    }
}