package ir.markazandroid.widget.clock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import ir.markazandroid.components.network.JSONParser.annotations.JSON;
import ir.markazandroid.widget.TransparentWidget;

import static android.content.ContentValues.TAG;

/**
 * Created by Damian on 22/04/2017.
 */
public class DigitalClock extends TransparentWidget {

    private Typeface custom_font;
    private TextPaint textPaintDate = new TextPaint();
    private TextPaint textPaintTime = new TextPaint();
    private Paint paint = new Paint();
    private Timer timer;
    private DigitalClock.Params params;
    private int color;

    public DigitalClock(Context context) {
        super(context);
    }


    /**
     * this funcition config ui property of this module base on
     * its json
     *
     * @param extras corresponding json for config class ui property
     */
    public void initWidget(JSONObject extras) {

        //load the digital font
        custom_font = Typeface.createFromAsset(getContext().getApplicationContext().getAssets(), "fonts/digital.ttf");

        timer = new Timer();
        params = parser.get(DigitalClock.Params.class, extras);

        color = params.get_text_color_int();
        TimerTask UpdateView = new DigitalClock.UpdateView();
        timer.scheduleAtFixedRate(UpdateView, 0, 1000);
        if (!isBackgroundSet())
            setBackgroundColor(Color.WHITE);
        else
            setBackgroundColor(Color.TRANSPARENT);


    }
    public static class Params {


        private String textColor ;


        @JSON
        public String getTextColor() {
            return textColor;
        }



        public void setTextColor(String textColor)
        {

            this.textColor   = textColor;

        }
        public int get_text_color_int()
        {
            if(textColor == null||textColor.equals(""))
            {
                return Color.argb(255,0,0,0);
            }
            int result = 0;

            result   =  Integer.parseInt( textColor.substring(1,textColor.length()),16);
            result+=Color.BLACK;
            return result;
        }



    }
    class UpdateView extends TimerTask {


        public void run() {
            postInvalidate();

            //calculate the new position of myBall
        }
    }

    /**
     * this method diallocate al resurces that allocated durng runing process
     */
    public void disposeWidget() {
        timer.cancel();
    }


    //TODO draw doesnt get called at all
    @Override
    public void draw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.draw(canvas);

        //anti aliasing
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        //draw background
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        canvas.drawPaint(paint);

        //get screen size & set widths, heights, others
        int x = getWidth();
        int y = getHeight();
        int center_y = y / 2;


        //get date & time
        int[] date_time = getDateTime();


        //show date
        textPaintDate.setTextSize((int) (x / 4));
        textPaintDate.setTypeface(custom_font);
        textPaintDate.setColor(color);
        String re[]= addLeadingZerosforDate(date_time[0],date_time[1]+1,date_time[2]);
        Rect bounds = new Rect();
        String upper = re[2] + ":" + re[1] + ":" + re[0];
        textPaintDate.getTextBounds(upper, 0, upper.length(), bounds);
        int x_pos = (x - bounds.width()) / 2;
        int y_pos = bounds.height();
        canvas.drawText(upper, x_pos, center_y-y_pos, textPaintDate);


        //show time
        String[] time_str = addLeadingZeros(date_time[4], date_time[5], date_time[6]);

        textPaintTime.setTextSize(x / 4);
        textPaintTime.setTypeface(custom_font);
        textPaintTime.setColor(color);

        String middle = time_str[0] + ":" + time_str[1] + ":" + time_str[2];
        bounds = new Rect();
        textPaintTime.getTextBounds("00:00:00", 0, "00:00:00".length(), bounds);
        //textPaintTime.getTextBounds(middle, 0, middle.length(), bounds); //<- dynamic positioning
        x_pos = (x - bounds.width()) / 2;
        canvas.drawText(middle, x_pos, center_y + y_pos, textPaintTime);


    }


    private String[] addLeadingZeros(int hours, int minutes, int seconds) {
        String hours_str, minutes_str, seconds_str;

        if (hours <= 9)
            hours_str = "0" + hours;
        else
            hours_str = String.valueOf(hours);

        if (minutes <= 9)
            minutes_str = "0" + minutes;
        else
            minutes_str = String.valueOf(minutes);

        if (seconds <= 9)
            seconds_str = "0" + seconds;
        else
            seconds_str = String.valueOf(seconds);

        String[] values = {hours_str, minutes_str, seconds_str};
        return values;


    }

    private String[] addLeadingZerosforDate(int year, int mounth, int day) {
        String day_str, mounth_str, year_str;

        if (day <= 9)
            day_str = "0" + day;
        else
            day_str = String.valueOf(day);

        if (mounth <= 9)
            mounth_str = "0" + mounth;
        else
            mounth_str = String.valueOf(mounth);

        if (year <= 9)
            year_str = "000" + year;
        else if (year <= 99)
            year_str = "00" + year;
        else if (year <= 999)
            year_str = "0" + year;
        else
            year_str = String.valueOf(year);
        String[] values = {day_str, mounth_str, year_str};
        return values;


    }

    private Rect measureString(String text, TextPaint tp) {

        Rect bounds = new Rect();
        tp.getTextBounds(text, 0, text.length(), bounds);

        return bounds; // return width & height of string
    }


    private int[] getDateTime() {

        //get date & time
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int day_of_week = calendar.get(Calendar.DAY_OF_WEEK);
        int ss = calendar.get(Calendar.SECOND);
        int mm = calendar.get(Calendar.MINUTE);
        int hh = calendar.get(Calendar.HOUR_OF_DAY);

        int[] values = {year, month, day, day_of_week, hh, mm, ss};
        return values;

    }


}