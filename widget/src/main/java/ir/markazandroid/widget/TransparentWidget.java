package ir.markazandroid.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;

import ir.markazandroid.components.ComponentContainer;
import ir.markazandroid.components.model.EFile;
import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.components.network.downloader.MasterDownloader;

public abstract class TransparentWidget extends FrameLayout implements Widget {


    protected final Parser parser;
    protected final MasterDownloader masterDownloader;
    private final ImageView bgImageView;
    private boolean backgroundSet;

    private EFile bgFile;

    public TransparentWidget(@NonNull Context context) {
        super(context);
        parser = ((ComponentContainer) context.getApplicationContext()).getParser();
        masterDownloader = ((ComponentContainer) context.getApplicationContext()).getMasterDownloader();
        bgImageView = new ImageView(getContext());
        addView(bgImageView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    public void init(JSONObject extras) {

        //init background
        if (extras != null) {
            if (extras.optJSONObject("bgImage") != null) {
                bgFile = parser.get(EFile.class, extras.optJSONObject("bgImage"));
                masterDownloader.giveMeFile(bgFile, this,
                        (MasterDownloader.EFileCacheFetchListener) this::setImageBg,
                        (MasterDownloader.EFileNetFetchListener) (file, cached) -> {
                            if (!cached) setImageBg(file);
                        });
                backgroundSet = true;
            } else if (extras.opt("bgColor") != null) {
                int bgcolor = extras.optInt("bgColor");
                bgImageView.setImageDrawable(new ColorDrawable(bgcolor));
                backgroundSet = true;
            } else
                bgImageView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        initWidget(extras);
    }

    public void setImageBg(File file) {
        Log.e("TG", "setImageBg: ");
        Picasso.get().load(file)
                .fit()
                .noFade()
                .into(bgImageView);
        backgroundSet = true;
    }

    @Override
    public void dispose() {
        disposeWidget();
        if (bgFile != null)
            masterDownloader.removeListenerByTag(bgFile, this);
    }

    protected abstract void initWidget(JSONObject extras);

    protected abstract void disposeWidget();

    public boolean isBackgroundSet() {
        return backgroundSet;
    }
}
