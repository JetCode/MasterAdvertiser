package  ir.markazandroid.widget;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;
import org.json.JSONObject;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ir.markazandroid.components.model.EFile;
import ir.markazandroid.components.network.JSONParser.annotations.JSON;
import ir.markazandroid.components.network.downloader.MasterDownloader;

import static android.content.ContentValues.TAG;
import static android.view.View.MeasureSpec.UNSPECIFIED;

//10000
//50000
//90000 speed of
public class ScrollTextView extends TransparentWidget  {

    private boolean isUserScrolling, isPaused, stop;
    //private final Activity activity;
    private  Context context;
    private int duration;
    private int pixelYOffSet =3;
    private boolean m_bIsVertical = false;
    private boolean m_bIsPersian = false;
    private int speed =100;
    private int width;
    private  int height;
    private int pixelCount;
    private final TextView textView;
    private ScrollTextView.Params params;
    /**
     * Creates a vertically auto scrolling marquee of a TextView within an
     * Activity. The (long) duration in milliseconds between calls to the next
     * scrollBy(0, pixelYOffSet). Defaults to 65L. The (int) amount of Y pixels
     * to scroll by defaults to 1.
     */
//    public VerticalMarqueeTextView(Context context, AttributeSet attrs,
//                                   int defStyle) {
//        super(context, attrs, defStyle);
//
//        this.activity = (Activity) context;
//        textView = new TextView(context);
//        addView(textView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
//        setVisibility(INVISIBLE);
//        init();
//    }

    /**
     * this funcition config ui property of this module base on
     * its json
     *
     * @param extras corresponding json for config class ui property
     */
    public void initWidget(JSONObject extras) {
        Log.e(TAG, "initWidget: " );
        if (!isBackgroundSet())
            textView.setBackgroundColor(Color.WHITE);
        else
            textView.setBackgroundColor(Color.TRANSPARENT);


        params = parser.get(ScrollTextView.Params.class, extras);

        textView.setText(params.getText());
        textView.setTextColor(params.get_text_color_int());
        textView.setTextSize(params.getFontSize());
        this.setSpeed(params.getSpeed());
        if (params.getFont() != null) {
            masterDownloader.giveMeFile(params.getFont(), this,
                    (MasterDownloader.EFileCacheFetchListener) this::setFont,
                    (MasterDownloader.EFileNetFetchListener) (file, fromCache) -> {
                        if (!fromCache) setFont(file);
                    });
        }

        //  this.setText("So I'd like to change the android:fontFamily in Android but I don't see any pre-defined fonts in Android. How do I select one of the pre-defined ones? I don't really need to define my own TypeFace but all I need is something different from what it shows right now.");
        //this.setRndDuration(100000);

        // customize the TextView
        //textView.setEllipsize(null);
        textView.setLineSpacing(0, 1.2f);


        /*File font = new File(getContext().getFilesDir(),"font.ttf");
        try {
            FileUtils.copyInputStreamToFile(getContext().getAssets().open("AIranianSans.ttf"),font);
            setFont(font);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        startMarquee();

    }

    /**
     * this function correct input string base on widget with and string length in
     */
    private void correct_String_Lines_Base_On_Length() {
        int m_iNumeOfLines = textView.getText().toString().split("\n").length;
        String lof_str_Result = "";
        int lof_NumOf_Space = 0;
        String lof_str_Temp = "";
        int lof_String_Section_Width;
        String[] lof_string_part = textView.getText().toString().split("\n");
        int lof_iSpace_Width = calculateContentLenHorizontally("a a") - 2 * calculateContentLenHorizontally("a");
        //each line must be correct base on its length
        for (int i = 0; i < m_iNumeOfLines; i++) {
            //base on string length decide put \n in it to corect it
            //or no changes
            if (calculateContentLenHorizontally(lof_string_part[i]) > getWidth()) {
                //it is not good to break line inside a word so break after or before word
                int lof_Num_of_Space_In_Section = lof_string_part[i].split(" ").length;
                String[] lof_Str_Split_Space = lof_string_part[i].split(" ");
                String lof_Str_Space_Temp = "";
                String lof_Str_Space_Temp2 = "";
                String lof_Str_Section_Result_temp = "";
                boolean single_word = false;
                for (int k = 0; k < lof_Num_of_Space_In_Section; k++) {
                    lof_Str_Space_Temp += lof_Str_Split_Space[k];
                    lof_Str_Space_Temp += " ";
                    //calculate if string is no longer than widget with
                    if ((calculateContentLenHorizontally(lof_Str_Space_Temp) + lof_iSpace_Width) <= getWidth()) {


                    } else {
                        //here prevoues string htat is max lenghth that can be
                        //shown in text view without over flow is added to result
                        if(single_word)
                        {
                            lof_Str_Section_Result_temp += lof_Str_Space_Temp2;
                            lof_Str_Section_Result_temp += "\n";
                        }
                        else
                        {
                            lof_Str_Section_Result_temp +=  lof_Str_Split_Space[k];
                            lof_Str_Section_Result_temp += "\n";
                        }
//                        lof_Str_Section_Result_temp += lof_Str_Space_Temp2;
//                        lof_Str_Section_Result_temp += "\n";
                        lof_Str_Space_Temp2 = "";
                        lof_Str_Space_Temp = "";
                        if(single_word)
                        k--;
                        single_word = false;
                        continue;
                    }
                    lof_Str_Space_Temp2 += lof_Str_Split_Space[k];
                    lof_Str_Space_Temp2 += " ";
                    single_word = true;
                }
                if (lof_Str_Space_Temp != "") {
                    //reminding of string must be added to result
                    lof_Str_Section_Result_temp += lof_Str_Space_Temp2;
                }
                //after correct each line it must be added to final string
                lof_str_Result += lof_Str_Section_Result_temp;
                //split function delete its deliminator so we add it
                lof_str_Result += "\n";
            } else {
                lof_str_Result += lof_string_part[i];
                lof_str_Result += "\n";
            }

        }
        textView.setText(lof_str_Result);
    }



    /**
     * this method diallocate all resurces that allocated durng runing process
     */
    public void disposeWidget() {
        if (params.getFont() != null)
            masterDownloader.removeListenerByTag(params.getFont(), this);
        stopMarquee();
        Log.e(TAG, "disposeWidget:    stopMarquee();public void disposeWidget() {" );
    }

    /**
     *
     */
    public static class Params {
        private String text;

        private String textColor ;
        private int fontSize = 21;
        private boolean shouldScroll = false;
        private int speed = 100;

        private EFile font;

        @JSON
        public String getTextColor() {
            return textColor;
        }



        public void setTextColor(String textColor)
        {

            this.textColor   = textColor;

        }
        public int get_text_color_int()
        {
            int result = 0;

            result   =  Integer.parseInt( textColor.substring(1,textColor.length()),16);
            result+=Color.BLACK;
            return result;
        }
        @JSON
        public int getFontSize() {
            return fontSize;
        }

        public void setFontSize(int fontSize) {
            this.fontSize = fontSize;
        }


        public void setShouldScroll(boolean shouldScroll) {
            this.shouldScroll = shouldScroll;
        }

        @JSON
        public boolean getShouldScroll() {
            return shouldScroll;
        }

        @JSON
        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @JSON(classType = JSON.CLASS_TYPE_OBJECT, clazz = EFile.class)
        public EFile getFont() {
            return font;
        }

        public void setFont(EFile font) {
            this.font = font;
        }

        @JSON
        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }
    }

    public void getDimention() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        textView.getDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
    }

    /**
     * this function set font base on what we downloaded from server
     *
     * @param path
     */
    public void setFont(File path) {
        stopMarquee();
        textView.setTypeface(Typeface.createFromFile(path));
        init();
        startMarquee();

    }

    /**
     * Creates a vertically auto scrolling marquee of a TextView within an
     * Activity. The (long) duration in milliseconds between calls to the next
     * scrollBy(0, pixelYOffSet). Defaults to 65L. The (int) amount of Y pixels
     * to scroll by defaults to 1.
     */
//    public VerticalMarqueeTextView(Context context, AttributeSet attrs) {
//
//        super(context, attrs);
//        this.activity = (Activity) context;
//        textView = new TextView(context);
//        addView(textView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
//        setVisibility(INVISIBLE);
//        init();
//    }

    /**
     * Creates a vertically auto scrolling marquee of a TextView within an
     * Activity. The (long) duration in milliseconds between calls to the next
     * scrollBy(0, pixelYOffSet). Defaults to 65L. The (int) amount of Y pixels
     * to scroll by defaults to 1.
     */
    public ScrollTextView(Context context) {
        super(context);
        Log.e(TAG, "VerticalMarqueeTextView: " );
        //this.activity = (Activity) context;
        this.context = context;
        textView = new TextView(context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        params.gravity=Gravity.CENTER;
        addView(textView,params);
        textView.setVisibility(INVISIBLE);
        init();

    }

    public static final Pattern RTL_CHARACTERS =
            Pattern.compile("[\u0600-\u06FF\u0750-\u077F\u0590-\u05FF\uFE70-\uFEFF]");

    boolean patternMatch(String s) {

        Matcher matcher = RTL_CHARACTERS.matcher(s);
        if (matcher.find()) {
            return true;  // it's RTL
        }
        return false;
    }


    /**
     * Initialize fields and start the marquee.
     */
    private void init() {
        Log.e(TAG, "init: " );
        //setDuration(65l);
        //setPixelYOffSet(5);
        textView.setMovementMethod(new ScrollingMovementMethod());
        isUserScrolling = isPaused = stop = false;



    }

    public void setSpeed(int speed) {
        if (speed == 0) {
            this.speed = 100;
        }
        this.speed = speed;
        setPixelYOffSet(speed/100);
    }

    /**
     * @return Returns the (long) duration in milliseconds between calls to the
     * next scrollBy(0, pixelYOffSet).
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration Sets the (long) duration in milliseconds between calls to the
     *                 next scrollBy(0, pixelYOffSet). Defaults to 65L if value is
     *                 less than or equal to 0.
     */
    public void setDuration(int duration) {
        if (duration <= 0) {
            Log.e(TAG, "setDuration: les than zero ");
            this.duration = 1;
        } else {
            this.duration = duration;
            Log.e(TAG, "setDuration:  "+duration);
        }

    }

    /**
     * @return Returns the (int) amount of Y pixels to scroll by.
     */
    public int getPixelYOffSet() {
        return pixelYOffSet;
    }

    /**
     * @param pixelYOffSet Sets the (int) amount of Y pixels to scroll by. Defaults to 1
     *                     if value is less.
     */
    public void setPixelYOffSet(int pixelYOffSet) {
        if (pixelYOffSet < 1) {
            this.pixelYOffSet = 1;
        } else {
            this.pixelYOffSet = pixelYOffSet;
        }
    }
private float calculateHeight()
{

    return textView.getPaint().getFontMetrics().bottom - textView.getPaint().getFontMetrics().top;
}
    private void textViewNotDrawn() {


         int m_iNumeOfLines = (textView.getText() + "").split("\n").length;
       // m_iNumeOfLines +=   calculateContentLenHorizontally()/textView.getWidth();;

        m_bIsVertical = m_iNumeOfLines > 1;
        if (m_bIsVertical) {
            // Checks to see if VMTV has been drawn.
            // In theory line count should be greater than 0 if drawn.
            Log.e(TAG, "textViewNotDrawn: "+textView.getLineCount());
            if (m_iNumeOfLines > 0) {
                // Calculate the total pixel height that needs to be
                // scrolled.
                // May need additional calculations if there is
                // additional padding.
//                        Log.e(TAG, "run:  line height with formaula  " +calculateContentLenVertically());
//                        Log.e(TAG, "run:  line count  " +(this).getLineCount());
//                        Log.e(TAG, "run:  line height  " +(this).getLineHeight());

                pixelCount = textView.getLineHeight()
                        * m_iNumeOfLines;
                textView.measure(0, 0);       //must call measure!
                textView.getMeasuredHeight(); //get width
                textView.getMeasuredWidth();

                pixelCount = textView.getLineHeight()  * m_iNumeOfLines;
                pixelCount=  textView.getMeasuredHeight();
                pixelCount  = m_iNumeOfLines * textView.getLineHeight();
                Log.e(TAG, "pixelCount: "+pixelCount);
                Log.e(TAG, "textViewNotDrawn: "+textView.getLineCount());
                Log.e(TAG, "   textView.getMeasuredHeight(): "+   textView.getMeasuredHeight());
                Log.e(TAG, "    textView.getMeasuredWidth();: "+    textView.getMeasuredWidth());
                Log.e(TAG, "    textView.calculateContentLenHorizontally;: "+   calculateContentLenHorizontally());
                Log.e(TAG, "    textView.calculateHeight();: "+    calculateHeight());
                textView.setSingleLine(false);
                textView.setGravity(Gravity.CENTER);
                int height = textView.getHeight();
                getDimention();
                setDuration(this.height / speed);
//                        Log.e(TAG, "run: duration vertical " + duration);
//                        Log.e(TAG, "run: speed vertical " + String.valueOf(((  this.height )/duration)));
//                        Log.e(TAG, "run: pix vertical " + String.valueOf( this.height ));
//                        Log.e(TAG, "run: pix vertical pixel count  " + String.valueOf(pixelCount ));
//                        Log.e(TAG, "run: pix vertical pixel height  " + String.valueOf( this.height ));
                textView.scrollTo(0,
                        -height);
            }
        } else {
            //setSelected(true);
            textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            textView.setHorizontallyScrolling(true);
            // Checks to see if VMTV has been drawn.
            // In theory line count should be greater than 0 if drawn.

            // Calculate the total pixel height that needs to be
            // scrolled.
            // May need additional calculations if there is
            // additional padding.
            pixelCount = calculateContentLenHorizontally();
            //Log.e(TAG, "run: duration hoizon calculateContentLenHorizontally  " + pixelCount);


            int width = textView.getWidth();
            //Log.e(TAG, "run: duration hoizon getWidth  " + width);
            textView.setMaxLines(1);
            if (patternMatch(textView.getText() + "")) {
                textView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
                textView.scrollTo(pixelCount,
                        0);
                m_bIsPersian = true;


            } else {
                textView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
                textView.scrollTo(-width,
                        0);


            }
            getDimention();
            setDuration(this.width / speed);
//                    Log.e(TAG, "run: duration hoizon  " + duration);
//                    Log.e(TAG, "run: speed hori " + (( this.width )/duration));
//                    Log.e(TAG, "run: pix hori " + String.valueOf( this.width ));
//                    Log.e(TAG, "run: pix count  hori " + pixelCount );
//                    Log.e(TAG, "run: pix width  hori " + this.width );

        }


        return;
    }

    /**
     * Starts the marquee. May only be called once.
     */

    private AutoScrollTextView asyncAutoScrollTextView;

    public void startMarquee() {

         int m_iNumeOfLines = (textView.getText() + "").split("\n").length;
//        int my_num_line = calculateContentLenHorizontally()/textView.getWidth();
//        m_iNumeOfLines+=my_num_line;
        m_bIsVertical = m_iNumeOfLines > 1;
        if (m_bIsVertical) {
            correct_String_Lines_Base_On_Length();
            Log.e(TAG, "run: textView.getLineHeight() " + textView.getLineHeight());
            Log.e(TAG, "run: textView.getLineCount() " + textView.getLineCount());
            Log.e(TAG, "run: textView.getHeight() " + textView.getHeight());
            textView.measure(UNSPECIFIED, UNSPECIFIED);       //must call measure!
            textView.getMeasuredHeight(); //get width
            textView.getMeasuredWidth();
            pixelCount = textView.getLineHeight()  * m_iNumeOfLines;
            pixelCount=  textView.getMeasuredHeight();
            m_iNumeOfLines = (textView.getText() + "").split("\n").length;
            int hright_from_this = m_iNumeOfLines * textView.getLineHeight();
            Log.e(TAG, "init:calculateContentLenHorizontally()/textView.getWidth();" + m_iNumeOfLines );
            Log.e(TAG, "init:calculateContentLenHorizontally()/hright_from_this" + hright_from_this);
            Log.e(TAG, "m_iNumeOfLines;" + m_iNumeOfLines );
            Log.e(TAG, "init:   textView.getMeasuredHeight(); "+textView.getMeasuredHeight() );
            Log.e(TAG, "init:   calculateContentLenVertically(); "+String.valueOf(m_iNumeOfLines*calculateContentLenVertically()));
            Log.e(TAG, "    textView.calculateHeight();: "+    calculateHeight());
            if ((  hright_from_this) > textView.getHeight()) {
                textViewNotDrawn();
                asyncAutoScrollTextView = new AutoScrollTextView();
                asyncAutoScrollTextView.execute();
            } else {
                textView.setGravity(Gravity.CENTER);
                textView.setVisibility(VISIBLE);
                //Log.e(TAG, "init:   startMarquee(); " );
            }
        } else {
            //Log.e(TAG, "init:   calculateContentLenHorizontally startMarquee(); getWidth "+getWidth() +"calculateContentLenHorizontally "+calculateContentLenHorizontally() );
            if (calculateContentLenHorizontally() > textView.getWidth()) {
                textViewNotDrawn();
                asyncAutoScrollTextView = new AutoScrollTextView();
                asyncAutoScrollTextView.execute();
            } else {
                //Log.e(TAG, "init:   stop setGravity(Gravity.CENTER);" );
                textView.setGravity(Gravity.CENTER);
                textView.setVisibility(VISIBLE);
            }
        }


    }

    /**
     * Stop the marquee. Cannot be restarted.
     */
    public void stopMarquee() {
        stop = true;
        if (asyncAutoScrollTextView != null && asyncAutoScrollTextView.asyncThread != null) {
            asyncAutoScrollTextView.asyncThread.interrupt();
            Log.e(TAG, "      if (asyncAutoScrollTextView != null && asyncAutoScrollTextVie " );
        }
    }

    /**
     * Pauses the marquee.
     */
    public void pauseMarquee() {
        isPaused = true;
    }

    /**
     * Resumes marquee from a call to pauseMarquee();
     */
    public void resumeMarquee() {
        isPaused = false;
    }

    /**
     * @return Returns true if paused (including if paused do to user manually
     * scrolling), false otherwise.
     */
    public boolean isPaused() {
        if (isPaused || isUserScrolling) {
            return true;
        }
        return false;
    }

    /**
     * calculate the scrolling length of the text in pixel
     *
     * @return the scrolling length in pixels
     */
    private int calculateContentLenHorizontally() {
        TextPaint tp = textView.getPaint();
        Rect rect = new Rect();
        String strTxt = textView.getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        int scrollingLen = rect.width();
        rect = null;
        return scrollingLen;
    }
    /**
     * calculate the scrolling length of the text in pixel
     *
     * @return the scrolling length in pixels
     */
    private int calculateContentLenHorizontally(String str) {
        TextPaint tp = textView.getPaint();
        Rect rect = new Rect();
        String strTxt =str;
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        int scrollingLen = rect.width();
        rect = null;
        return scrollingLen;
    }
    private int calculateContentLenVertically() {
        TextPaint tp = textView.getPaint();
        tp.getTextSize();
        Log.e(TAG, "calculateContentLenVertically: "+  tp.getTextSize() );
        Rect rect = new Rect();
        String strTxt = textView.getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        int scrollingLen = rect.height();
        rect = null;
        return scrollingLen;
    }

    private class AutoScrollTextView extends AsyncTask<Void, Void, Void> {

        private int pixelCount;
        private Thread asyncThread;

        @Override
        protected Void doInBackground(Void... params) {
            asyncThread = Thread.currentThread();
            // textViewNotDrawn();
            // Check to see if the VMTV has been drawn to get proper sizing.
//            while (textViewNotDrawn()) //{
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    return null;
//                }


            new Handler(context.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    textView.setVisibility(VISIBLE);
                }
            });




//            activity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//
//                    textView.setVisibility(VISIBLE);
//
//                }
//            });

            // Changing stop to true will permanently cancel autoscrolling.
            // Cannot be restarted.
            while (!stop) {


                new Handler(context.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        // Allows scrolling to resume after VMTV has been released.
                        if (!(textView).isPressed()
                                && isUserScrolling && !isPaused) {
                            isUserScrolling = false;
                        }
                    }
                });
//                activity.runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//
//                        // Allows scrolling to resume after VMTV has been released.
//                        if (!(textView).isPressed()
//                                && isUserScrolling && !isPaused) {
//                            isUserScrolling = false;
//                        }
//
//                    }
//                });


                while (!isUserScrolling && !stop && !isPaused) {
                    // Sleep duration amount between scrollBy pixelYOffSet
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        return null;
                    }
                    new Handler(context.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {

                            // If the user is manually scrolling pause the auto
                            // scrolling marquee
                            if ((textView).isPressed()) {

                                isUserScrolling = true;

                            } else { // Otherwise auto scroll marquee
                                if (m_bIsVertical) {
                                    // if VMTV has reached or exceeded the last
                                    // Y pixel scroll back to top
                                    if ((textView).getScrollY() >= ScrollTextView.this.pixelCount) {

                                        (textView).scrollTo(0,
                                                -textView.getHeight());

                                    } else { // Otherwise scroll by the pixelYOffSet

                                        (textView).scrollBy(0,
                                                pixelYOffSet);
                                    }
                                } else {


                                    if (m_bIsPersian) {
//                                        Log.e(TAG, "run: getScaleX " + (VerticalMarqueeTextView.this).getScrollX());
//                                        Log.e(TAG, "run: pixelCount " + pixelCount);
                                        // if VMTV has reached or exceeded the last
                                        // Y pixel scroll back to top
                                        if (((textView).getScrollX()) <= -textView.getWidth()) {
                                            Log.e(TAG, "run: pixelCount " + pixelCount);
                                            (textView).scrollTo(ScrollTextView.this.pixelCount,
                                                    0);

                                        } else { // Otherwise scroll by the pixelYOffSet

                                            (textView).scrollBy(-ScrollTextView.this.pixelYOffSet,
                                                    0);
                                        }
                                    } else {

                                        Log.e(TAG, "run: getScaleXx " + textView.getScrollX());

                                        // if VMTV has reached or exceeded the last
                                        // Y pixel scroll back to top
                                        if (((textView).getScrollX()) >= (ScrollTextView.this.pixelCount)) {
                                            Log.e(TAG, "run: pixelCountx " + pixelCount);
                                            (textView).scrollTo(-textView.getWidth(),
                                                    0);

                                        } else { // Otherwise scroll by the pixelYOffSet

                                            (textView).scrollBy(pixelYOffSet,
                                                    0);
                                        }
                                    }
                                }

                                (textView).invalidate();
                            }

                        }
                    });
//                    activity.runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//
//                            // If the user is manually scrolling pause the auto
//                            // scrolling marquee
//                            if ((textView).isPressed()) {
//
//                                isUserScrolling = true;
//
//                            } else { // Otherwise auto scroll marquee
//                                if (m_bIsVertical) {
//                                    // if VMTV has reached or exceeded the last
//                                    // Y pixel scroll back to top
//                                    if ((textView).getScrollY() >= VerticalMarqueeTextView.this.pixelCount) {
//
//                                        (textView).scrollTo(0,
//                                                -textView.getHeight());
//
//                                    } else { // Otherwise scroll by the pixelYOffSet
//
//                                        (textView).scrollBy(0,
//                                                pixelYOffSet);
//                                    }
//                                } else {
//
//
//                                    if (m_bIsPersian) {
////                                        Log.e(TAG, "run: getScaleX " + (VerticalMarqueeTextView.this).getScrollX());
////                                        Log.e(TAG, "run: pixelCount " + pixelCount);
//                                        // if VMTV has reached or exceeded the last
//                                        // Y pixel scroll back to top
//                                        if (((textView).getScrollX()) <= -textView.getWidth()) {
//                                            //Log.e(TAG, "run: pixelCount " + pixelCount);
//                                            (textView).scrollTo(VerticalMarqueeTextView.this.pixelCount,
//                                                    0);
//
//                                        } else { // Otherwise scroll by the pixelYOffSet
//
//                                            (textView).scrollBy(-VerticalMarqueeTextView.this.pixelYOffSet,
//                                                    0);
//                                        }
//                                    } else {
//
//                                        //Log.e(TAG, "run: getScaleXx " + (VerticalMarqueeTextView.this).getScrollX());
//
//                                        // if VMTV has reached or exceeded the last
//                                        // Y pixel scroll back to top
//                                        if (((textView).getScrollX()) >= (VerticalMarqueeTextView.this.pixelCount)) {
//                                            //Log.e(TAG, "run: pixelCountx " + pixelCount);
//                                            (textView).scrollTo(-textView.getWidth(),
//                                                    0);
//
//                                        } else { // Otherwise scroll by the pixelYOffSet
//
//                                            (textView).scrollBy(pixelYOffSet,
//                                                    0);
//                                        }
//                                    }
//                                }
//
//                                (textView).invalidate();
//                            }
//
//                        }
//                    });
                }
            }

            return null;
        }


        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

//        private boolean textViewNotDrawn() {
//
//            activity.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//                    final int m_iNumeOfLines = (getText() + "").split("\n").length;
//
//
//                    m_bIsVertical = m_iNumeOfLines > 1;
//                    if (m_bIsVertical) {
//                        // Checks to see if VMTV has been drawn.
//                        // In theory line count should be greater than 0 if drawn.
//                        if ((VerticalMarqueeTextView.this).getLineCount() > 0) {
//                            // Calculate the total pixel height that needs to be
//                            // scrolled.
//                            // May need additional calculations if there is
//                            // additional padding.
//                            //Log.e(TAG, "run:  line height with formaula  " +calculateContentLenVertically());
//                            //Log.e(TAG, "run:  line count  " +(VerticalMarqueeTextView.this).getLineCount());
//                            //Log.e(TAG, "run:  line height  " +(VerticalMarqueeTextView.this).getLineHeight());
//
//                            pixelCount = (VerticalMarqueeTextView.this).getLineHeight()
//                                    * (VerticalMarqueeTextView.this).getLineCount();
//                            isNotDrawn = false;
//                            setSingleLine(false);
//                            VerticalMarqueeTextView.this.setGravity(Gravity.CENTER);
//                            int height = VerticalMarqueeTextView.this.getHeight();
//                            getDimention();
//                            setDuration(((VerticalMarqueeTextView.this.height) / (speed)));
//                            //Log.e(TAG, "run: duration vertical " + duration);
//                            //Log.e(TAG, "run: speed vertical " + String.valueOf(((  VerticalMarqueeTextView.this.height )/duration)));
//                            //Log.e(TAG, "run: pix vertical " + String.valueOf( VerticalMarqueeTextView.this.height ));
//                            //Log.e(TAG, "run: pix vertical pixel count  " + String.valueOf(pixelCount ));
//                            //Log.e(TAG, "run: pix vertical pixel height  " + String.valueOf( VerticalMarqueeTextView.this.height ));
//                        }
//                    } else {
//                        //setSelected(true);
//                        setEllipsize(TextUtils.TruncateAt.MARQUEE);
//                        setHorizontallyScrolling(true);
//                        // Checks to see if VMTV has been drawn.
//                        // In theory line count should be greater than 0 if drawn.
//                        if ((VerticalMarqueeTextView.this).getLineCount() > 0) {
//                            // Calculate the total pixel height that needs to be
//                            // scrolled.
//                            // May need additional calculations if there is
//                            // additional padding.
//                            pixelCount = calculateContentLenHorizontally();
//                            isNotDrawn = false;
//
//                        }
//                        int width = VerticalMarqueeTextView.this.getWidth();
//                        setMaxLines(1);
//                        if (patternMatch(getText() + "")) {
//                            VerticalMarqueeTextView.this.setGravity(Gravity.LEFT);
//                            (VerticalMarqueeTextView.this).scrollTo(pixelCount,
//                                    0);
//                            m_bIsPersian = true;
//
//
//                        } else {
//                            VerticalMarqueeTextView.this.setGravity(Gravity.LEFT);
//                            (VerticalMarqueeTextView.this).scrollTo(-width,
//                                    0);
//                            m_bIsPersian = false;
//
//                        }
//                        getDimention();
//                        setDuration(((VerticalMarqueeTextView.this.width) / (speed)));
//                        //Log.e(TAG, "run: duration hoizon  " + duration);
//                        //Log.e(TAG, "run: speed hori " + (( VerticalMarqueeTextView.this.width )/duration));
//                        //Log.e(TAG, "run: pix hori " + String.valueOf( VerticalMarqueeTextView.this.width ));
//                        //Log.e(TAG, "run: pix count  hori " + pixelCount );
//                        //Log.e(TAG, "run: pix width  hori " + VerticalMarqueeTextView.this.width );
//
//                    }
//
//                }
//            });
//
//            return isNotDrawn;
//        }

    }

}