package ir.markazandroid.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

public class BlankWidget extends TextView implements Widget {
    public BlankWidget(Context context) {
        this(context, null);
    }

    public BlankWidget(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BlankWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


    }

    @Override
    public void init(JSONObject extras) {
        setGravity(Gravity.CENTER);
        setText("این ویجت موجود نیست!");
    }

    @Override
    public void dispose() {

    }
}
