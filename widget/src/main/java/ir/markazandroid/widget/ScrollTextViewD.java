package ir.markazandroid.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.animation.LinearInterpolator;
import android.widget.Scroller;
import android.widget.TextView;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import java.io.File;

import ir.markazandroid.components.model.EFile;
import ir.markazandroid.components.network.JSONParser.annotations.JSON;
import ir.markazandroid.components.network.downloader.MasterDownloader;

import static android.content.ContentValues.TAG;

//import android.support.v7.widget.AppCompatTextView;

@Deprecated
public class ScrollTextViewD extends TransparentWidget {

    private final TextView textView;

    // scrolling feature
    private Scroller mSlr;

    // milliseconds for a round of scrolling
    private int mRndDuration = 1000;

    // the X offset when paused
    private int mXPaused = 0;
    private int mYPaused = 0;

    // whether it's being paused
    private boolean mPaused = true;
    //This variable is used to indicates weather is first load or other than
    private boolean m_bIsFirstLoded = false;
    //this variable is used to indcate vertical show or horizon show
    private boolean m_bIsVertical = false;

    //this variable indicate if scroll enabled
    private boolean m_bMustScroll = false;
    //This variable is used to determine direction of scroll for persian or latin text
    private boolean m_bIsRightToLeft = true;
    //This variable is used to cont of textlines in vertical mode
    private int m_iNumeOfLines = 0;
    private Params params;


    public ScrollTextViewD(@NonNull Context context) {
        super(context);
        textView = new TextView(context);
        addView(textView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }


    /**
     * @return
     */

    public boolean isM_bMustScroll() {
        return m_bMustScroll;
    }

    /**
     * this function enable disable scroll
     *
     * @param m_bMustScroll
     */
    public void setM_bMustScroll(boolean m_bMustScroll) {
        this.m_bMustScroll = m_bMustScroll;
    }

    /**
     * this funcition config ui property of this module base on
     * its json
     *
     * @param extras corresponding json for config class ui property
     */
    public void initWidget(JSONObject extras) {

        if (!isBackgroundSet())
            setBackgroundColor(Color.WHITE);
        else
            setBackgroundColor(Color.TRANSPARENT);


        params = parser.get(Params.class, extras);

        textView.setText(params.getText());
        textView.setTextColor(params.getTextColor());
        textView.setTextSize(params.getFontSize());

        if (params.getFont() != null) {
            masterDownloader.giveMeFile(params.getFont(), this,
                    (MasterDownloader.EFileCacheFetchListener) this::setFont,
                    (MasterDownloader.EFileNetFetchListener) (file, fromCache) -> {
                        if (!fromCache) setFont(file);
                    });
        }

        //  this.setText("So I'd like to change the android:fontFamily in Android but I don't see any pre-defined fonts in Android. How do I select one of the pre-defined ones? I don't really need to define my own TypeFace but all I need is something different from what it shows right now.");
        this.setRndDuration(100000);
        this.setM_bMustScroll(params.getShouldScroll());
        // customize the TextView
        textView.setEllipsize(null);
        textView.setLineSpacing(0, 1.2f);
        if (m_bMustScroll) {
            m_bIsFirstLoded = true;
            m_bIsRightToLeft = false;
            this.startScroll();
        } else {
            textView.setGravity(Gravity.CENTER);
        }
        /*File font = new File(getContext().getFilesDir(),"font.ttf");
        try {
            FileUtils.copyInputStreamToFile(getContext().getAssets().open("AIranianSans.ttf"),font);
            setFont(font);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }

    /**
     * this method diallocate all resurces that allocated durng runing process
     */
    public void disposeWidget() {
        if (params.getFont() != null)
            masterDownloader.removeListenerByTag(params.getFont(), this);
    }

    /**
     *
     */
    public static class Params {
        private String text;
        private int textColor = Color.BLACK;
        private int fontSize = 21;
        private boolean shouldScroll = false;
        private int speed = 100;

        private EFile font;

        @JSON
        public int getTextColor() {
            return textColor;
        }

        public void setTextColor(int textColor) {
            this.textColor = textColor;
        }

        @JSON
        public int getFontSize() {
            return fontSize;
        }

        public void setFontSize(int fontSize) {
            this.fontSize = fontSize;
        }


        public void setShouldScroll(boolean shouldScroll) {
            this.shouldScroll = shouldScroll;
        }

        @JSON
        public boolean getShouldScroll() {
            return shouldScroll;
        }

        @JSON
        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @JSON(classType = JSON.CLASS_TYPE_OBJECT, clazz = EFile.class)
        public EFile getFont() {
            return font;
        }

        public void setFont(EFile font) {
            this.font = font;
        }

        @JSON
        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }
    }
    /**
     * this function set font base on what we downloaded from server
     *
     * @param path
     */
    public void setFont(File path) {
        Log.e(TAG, "setFont: Called");
        textView.setTypeface(Typeface.createFromFile(path));
    }

    /**
     * begin to scroll the text from the original position
     */
    public void startScroll() {
        // begin from the very right side
        mXPaused = -1 * getWidth();
        mYPaused = -1 * getHeight();
        // assume it's paused
        mPaused = true;

        m_iNumeOfLines = textView.getText().toString().split("\n").length;
        String[] lof_string_part = textView.getText().toString().split("\n");

        m_bIsVertical = m_iNumeOfLines > 1;

        int lof_iSpace_Width = calculateContentLenHorizontally("a");

        Log.e(TAG, String.format("a lenth:%d ", lof_iSpace_Width));

        Log.e(TAG, String.format("aa lenth:%d ", calculateContentLenHorizontally("a a")));
        Log.e(TAG, String.format("aaa lenth:%d ", calculateContentLenHorizontally("a  a")));
        Log.e(TAG, String.format("aaaa lenth:%d ", calculateContentLenHorizontally("a    a")));
        Log.e(TAG, String.format("aaaaa lenth:%d ", calculateContentLenHorizontally("     ")));
        Log.e(TAG, String.format("aaaaaa lenth:%d ", calculateContentLenHorizontally("      ")));
        if (m_bIsVertical) {
            textView.setGravity(Gravity.LEFT);


            textView.setSingleLine(false);
            correct_String_Lines_Base_On_Length();
            m_iNumeOfLines = textView.getText().toString().split("\n").length;
            int lof_iWidth = getWidth();
//            String lof_str_Result = "";
//            int lof_NumOf_Space = 0;
//            String lof_str_Temp = "";
//            int lof_String_Section_Width;
//            for( int i=0 ; i < m_iNumeOfLines ; i++)
//            {
//                lof_String_Section_Width = calculateContentLenHorizontally(lof_string_part[i]);
//
//                lof_NumOf_Space= 0;
//                if(lof_String_Section_Width <= lof_iWidth)
//                {
//                    lof_str_Temp="";
//                    lof_NumOf_Space = (int)((getWidth() - lof_String_Section_Width)/(2*lof_iSpace_Width));
//
//                    for(int j= 0 ; j < lof_NumOf_Space;j++)
//                    {
//                        lof_str_Temp+="a";
//                    }
//                    lof_str_Result+=lof_str_Temp;
//                    lof_str_Result+=lof_string_part[i];
////                    lof_str_Temp = "";
////                    for(int j= 0 ; j < lof_NumOf_Space;j++)
////                    {
////                        lof_str_Temp+="a";
////                    }
//                    lof_str_Result+="\n";
//
//                }
//                else
//                {
//
//                }
//            }
//            Log.e(TAG, String.format("#############before getStartY:%s ", lof_str_Result));
//            this.setText(lof_str_Result);
//


            resumeScrollVertically();
        } else {
            textView.setMovementMethod(new ScrollingMovementMethod());
            textView.setGravity(Gravity.LEFT);
            textView.setSingleLine(true);
            resumeScrollHorizontally();
        }


    }

    /**
     * this function correct input string base on widget with and string length in
     */
    private void correct_String_Lines_Base_On_Length() {
        m_iNumeOfLines = textView.getText().toString().split("\n").length;
        String lof_str_Result = "";
        int lof_NumOf_Space = 0;
        String lof_str_Temp = "";
        int lof_String_Section_Width;
        String[] lof_string_part = textView.getText().toString().split("\n");
        int lof_iSpace_Width = calculateContentLenHorizontally("a a") - 2 * calculateContentLenHorizontally("a");
        //each line must be correct base on its length
        for (int i = 0; i < m_iNumeOfLines; i++) {
            //base on string length decide put \n in it to corect it
            //or no changes
            if (calculateContentLenHorizontally(lof_string_part[i]) > getWidth()) {
                //it is not good to break line inside a word so break after or before word
                int lof_Num_of_Space_In_Section = lof_string_part[i].split(" ").length;
                String[] lof_Str_Split_Space = lof_string_part[i].split(" ");
                String lof_Str_Space_Temp = "";
                String lof_Str_Space_Temp2 = "";
                String lof_Str_Section_Result_temp = "";
                for (int k = 0; k < lof_Num_of_Space_In_Section; k++) {
                    lof_Str_Space_Temp += lof_Str_Split_Space[k];
                    lof_Str_Space_Temp += " ";
                    //calculate if string is no longer than widget with
                    if ((calculateContentLenHorizontally(lof_Str_Space_Temp) + lof_iSpace_Width) <= getWidth()) {


                    } else {
                        //here prevoues string htat is max lenghth that can be
                        //shown in text view without over flow is added to result
                        lof_Str_Section_Result_temp += lof_Str_Space_Temp2;
                        lof_Str_Section_Result_temp += "\n";
                        lof_Str_Space_Temp2 = "";
                        lof_Str_Space_Temp = "";
                        k--;
                        continue;
                    }
                    lof_Str_Space_Temp2 += lof_Str_Split_Space[k];
                    lof_Str_Space_Temp2 += " ";
                }
                if (lof_Str_Space_Temp != "") {
                    //reminding of string must be added to result
                    lof_Str_Section_Result_temp += lof_Str_Space_Temp2;
                }
                //after correct each line it must be added to final string
                lof_str_Result += lof_Str_Section_Result_temp;
                //split function delete its deliminator so we add it
                lof_str_Result += "\n";
            } else {
                lof_str_Result += lof_string_part[i];
                lof_str_Result += "\n";
            }

        }
        textView.setText(lof_str_Result);
    }

    private void verticalCenter() {

    }

    /**
     * TOdo THIS FUNCTION IS FAKE AND MUST BE DELETED
     * resume the scroll from the pausing point
     */
    public void resumeMyScroll() {

        if (!mPaused) return;
        // Do not know why it would not scroll sometimes
        // if setHorizontallyScrolling is called in constructor.
        textView.setHorizontallyScrolling(true);
        // use LinearInterpolator for steady scrolling
        mSlr = new Scroller(this.getContext(), new LinearInterpolator());
        textView.setScroller(mSlr);
        Log.e(TAG, String.format("getwith:%d ", -mXPaused));
        int scrollingLen = calculateContentLenHorizontally();
        Log.e(TAG, String.format("scrollingLen:%d ", scrollingLen));
        int distance = scrollingLen - (mXPaused);
        mSlr.computeScrollOffset();
        Log.e(TAG, String.format("before getCurrX:%d ", mSlr.getCurrX()));
        Log.e(TAG, String.format("before getCurrY:%d ", mSlr.getCurrY()));
        Log.e(TAG, String.format("before getFinalX:%d ", mSlr.getFinalX()));
        Log.e(TAG, String.format("before getFinalY:%d ", mSlr.getFinalY()));
        Log.e(TAG, String.format("before getStartX:%d ", mSlr.getStartX()));
        Log.e(TAG, String.format("before getStartY:%d ", mSlr.getStartY()));
        mSlr.startScroll(-2 * getWidth() - 1200, /*(scrollingLen + getWidth())*/0, scrollingLen, 0, 2000);
        mSlr.computeScrollOffset();
        Log.e(TAG, String.format("after getCurrX:%d ", mSlr.getCurrX()));
        Log.e(TAG, String.format("after getCurrY:%d ", mSlr.getCurrY()));
        Log.e(TAG, String.format("after getFinalX:%d ", mSlr.getFinalX()));
        Log.e(TAG, String.format("after getFinalY:%d ", mSlr.getFinalY()));
        Log.e(TAG, String.format("after getStartX:%d ", mSlr.getStartX()));
        Log.e(TAG, String.format("after getStartY:%d ", mSlr.getStartY()));


//        if (m_bIsRightToLeft) {
//
//            Log.e(TAG, "IF  m_bIsRIHT TO LEFT : ");
//
//            Log.e(TAG, "else m_bIsFirstLoded: ");
//            if (m_bIsFirstLoded) {
//                Log.e(TAG, " if(m_bIsSecoundLoded): ");
//                mSlr.startScroll(0, 0, scrollingLen, 0, (int) ((mRndDuration * (scrollingLen)) / (scrollingLen + getWidth())));
//                m_bIsFirstLoded = false;
//            } else {
//                Log.e(TAG, " else (m_bIsSecoundLoded): ");
//                mSlr.startScroll(-getWidth(), 0, scrollingLen + getWidth(), 0, mRndDuration);
//            }
//
//
//        } else {
//            Log.e(TAG, "ELSE m_bIsRIHT TO LEFT : ");
//            Log.e(TAG, "else m_bIsFirstLoded: ");
//            if (m_bIsFirstLoded) {
//                Log.e(TAG, " if(m_bIsSecoundLoded): ");
//                mSlr.startScroll(scrollingLen - getWidth(), 0, -scrollingLen, 0, (int) ((scrollingLen * mRndDuration) / (scrollingLen + getWidth())));
//                m_bIsFirstLoded = false;
//            } else {
//                Log.e(TAG, " else (m_bIsSecoundLoded): ");
//                mSlr.startScroll(scrollingLen, 0, -1 * (scrollingLen + getWidth()), 0, mRndDuration);
//            }
//        }
        invalidate();
        mPaused = false;
    }


    /**
     * resume the scroll from the pausing point
     */
    public void resumeScrollHorizontally() {

        if (!mPaused) return;
        // Do not know why it would not scroll sometimes
        // if setHorizontallyScrolling is called in constructor.
        textView.setHorizontallyScrolling(true);
        // use LinearInterpolator for steady scrolling
        mSlr = new Scroller(this.getContext(), new LinearInterpolator());
        textView.setScroller(mSlr);
        Log.e(TAG, String.format("mXPaused:%d ", mXPaused));
        int scrollingLen = calculateContentLenHorizontally();
        Log.e(TAG, String.format("scrollingLen:%d ", scrollingLen));
        int distance = scrollingLen - (mXPaused);
        Log.e(TAG, String.format("distance:%d ", distance));
        if (m_bIsRightToLeft) {


            Log.e(TAG, "IF  m_bIsRIHT TO LEFT : ");

            Log.e(TAG, "else m_bIsFirstLoded: ");
            if (m_bIsFirstLoded) {
                Log.e(TAG, " if(m_bIsSecoundLoded): ");
                //mSlr.startScroll(0, 0, scrollingLen, 0, (mRndDuration * (scrollingLen)) / (scrollingLen + getWidth()));
                mSlr.startScroll(0, 0, scrollingLen, 0, (int) (scrollingLen / ((float) params.speed / 1000)));
                m_bIsFirstLoded = false;
            } else {
                Log.e(TAG, " else (m_bIsSecoundLoded): ");
                //mSlr.startScroll(-getWidth(), 0, scrollingLen + getWidth(), 0, mRndDuration);
                mSlr.startScroll(-getWidth(), 0, scrollingLen + getWidth(), 0, (int) ((scrollingLen + getWidth()) / ((float) params.speed / 1000)));
            }


        } else {
            Log.e(TAG, "ELSE m_bIsRIHT TO LEFT : ");
            Log.e(TAG, "else m_bIsFirstLoded: ");
            if (m_bIsFirstLoded) {
                Log.e(TAG, " if(m_bIsSecoundLoded): ");
                mSlr.startScroll(scrollingLen - getWidth(), 0, -scrollingLen, 0, (int) (scrollingLen / ((float) params.speed / 1000)));
                //mSlr.startScroll(scrollingLen - getWidth(), 0, -scrollingLen, 0, (scrollingLen * mRndDuration) / (scrollingLen + getWidth()));
                m_bIsFirstLoded = false;
            } else {
                Log.e(TAG, " else (m_bIsSecoundLoded): ");
                mSlr.startScroll(scrollingLen, 0, -1 * (scrollingLen + getWidth()), 0, (int) ((scrollingLen + getWidth()) / ((float) params.speed / 1000)));
                //mSlr.startScroll(scrollingLen, 0, -1 * (scrollingLen + getWidth()), 0, mRndDuration);
            }
        }
        invalidate();
        mPaused = false;
    }

    /**
     * TODO THIS FUNCTION HAS PROBLEM ABOUT VERTICAL IN RIGHT GRAVITY
     * resume the scroll from the pausing point
     */
    public void resumeScrollVertically() {

        if (!mPaused) return;
        // Do not know why it would not scroll sometimes
        // if setHorizontallyScrolling is called in constructor.
        textView.setHorizontallyScrolling(true);
        // use LinearInterpolator for steady scrolling
        mSlr = new Scroller(this.getContext(), new LinearInterpolator());
        textView.setScroller(mSlr);

        int scrollingLen = calculateContentLenVertically();
        Log.e(TAG, String.format("scrollingLenhORIZON:%d ", scrollingLen));
        Log.e(TAG, String.format("getHeight:%d ", getHeight()));
        Log.e(TAG, String.format("getHeightLine:%d ", textView.getLineHeight()));
        if (m_bIsFirstLoded) {
            mSlr.startScroll(0, 0, 0, scrollingLen * m_iNumeOfLines, (int) ((scrollingLen * m_iNumeOfLines) / ((float) params.speed / 1000)));
            //mSlr.startScroll(0, 0, 0, scrollingLen * m_iNumeOfLines, (mRndDuration * scrollingLen * m_iNumeOfLines) / (scrollingLen * m_iNumeOfLines + getHeight()));
            m_bIsFirstLoded = false;
        } else {
            mSlr.startScroll(0, -getHeight(), 0, scrollingLen * m_iNumeOfLines + getHeight(), (int) ((scrollingLen * m_iNumeOfLines + getHeight()) / ((float) params.speed / 1000)));
            //mSlr.startScroll(0, -getHeight(), 0, scrollingLen * m_iNumeOfLines + getHeight(), mRndDuration);
        }
//        m_iTest +=(int)(getWidth()/2);
//        Log.e(TAG, String.format(" m_iTest +=scrollingLen;:%d ", m_iTest));
        invalidate();
        mPaused = false;
    }


    /**
     * calculate the scrolling length of the text in pixel
     *
     * @return the scrolling length in pixels
     */
    private int calculateContentLenHorizontally() {
        TextPaint tp = textView.getPaint();
        Rect rect = new Rect();
        String strTxt = textView.getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        int scrollingLen = rect.width();
        rect = null;
        return scrollingLen;
    }

    /**
     * calculate the scrolling length of the text in pixel
     *
     * @param arg_StrTextview STRING THAT IS PASSED  TO FUNCTION FOR CALCULATE
     *                        LENGTH
     * @return the scrolling length in pixels
     */
    private int calculateContentLenHorizontally(String arg_StrTextview) {
        TextPaint tp = textView.getPaint();
        Rect rect = new Rect();

        tp.getTextBounds(arg_StrTextview, 0, arg_StrTextview.length(), rect);
        int scrollingLen = rect.width();
        rect = null;
        return scrollingLen;
    }

    /**
     * calculate the scrolling length of the text in pixel
     *
     * @return the scrolling length in pixels
     */
    private int calculateContentLenVertically() {
        TextPaint tp = textView.getPaint();
        Rect rect = new Rect();
        String strTxt = textView.getText().toString();
        tp.getTextBounds(strTxt, 0, strTxt.length(), rect);
        int scrollingLen = rect.height();
        rect = null;
        return scrollingLen;
    }

    /**
     * pause scrolling the text
     */
    public void pauseScroll() {
        if (null == mSlr) return;
        if (mPaused)
            return;
        mPaused = true;
        // abortAnimation sets the current X to be the final X,
        // and sets isFinished to be true
        // so current position shall be saved
        mXPaused = mSlr.getCurrX();
        mSlr.abortAnimation();
    }

    @Override
    /*
     * override the computeScroll to restart scrolling when finished so as that
     * the text is scrolled forever
     */
    public void computeScroll() {
        super.computeScroll();

        if (null == mSlr) return;

        if (mSlr.isFinished() && (!mPaused)) {
//            mSlr.computeScrollOffset();
//            Log.e(TAG, String.format("afterStop getCurrY:%d ", mSlr.getCurrY()));
//            Log.e(TAG, String.format("afterStop getCurrX:%d ", mSlr.getCurrX()));
//            Log.e(TAG, String.format("afterStop getFinalX:%d ", mSlr.getFinalX()));
//            Log.e(TAG, String.format("afterStop getFinalY:%d ", mSlr.getFinalY()));
//            Log.e(TAG, String.format("afterStop getStartX:%d ", mSlr.getStartX()));
//            Log.e(TAG, String.format("afterStop getStartY:%d ", mSlr.getStartY()));
            this.startScroll();

        }
    }

    /**
     * @return
     */
    public int getRndDuration() {
        return mRndDuration;
    }

    /**
     * @param duration
     */
    public void setRndDuration(int duration) {
        this.mRndDuration = duration;
    }

    /**
     * @return boolean weather scrol is paused
     */
    public boolean isPaused() {
        return mPaused;
    }
}

