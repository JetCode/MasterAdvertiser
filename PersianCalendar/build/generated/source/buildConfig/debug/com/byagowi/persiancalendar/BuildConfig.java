/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.byagowi.persiancalendar;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.byagowi.persiancalendar";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "com.byagowi.persiancalendar";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 588;
  public static final String VERSION_NAME = "5.8.8-master-0ecbb0b";
}
