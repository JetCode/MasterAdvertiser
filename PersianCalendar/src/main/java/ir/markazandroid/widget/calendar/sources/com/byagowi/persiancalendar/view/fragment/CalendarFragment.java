package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.byagowi.persiancalendar.R;
import com.byagowi.persiancalendar.databinding.CalendarsTabContentBinding;
import com.byagowi.persiancalendar.databinding.EventsTabContentBinding;
import com.byagowi.persiancalendar.databinding.FragmentCalendarBinding;
import com.byagowi.persiancalendar.databinding.OwghatTabContentBinding;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.Inflater;

import ir.markazandroid.widget.calendar.sources.calendar.AbstractDate;
import ir.markazandroid.widget.calendar.sources.calendar.CalendarType;
import ir.markazandroid.widget.calendar.sources.calendar.CivilDate;
import ir.markazandroid.widget.calendar.sources.calendar.DateConverter;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.Constants;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.adapter.CalendarAdapter;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.entity.AbstractEvent;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.entity.DeviceCalendarEvent;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.CalendarUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UIUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity.UnitecCalanader;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.sunrisesunset.SunView;
import ir.markazandroid.widget.calendar.sources.com.cepmuvakkit.times.posAlgo.SunMoonPosition;
import ir.markazandroid.widget.calendar.sources.com.github.praytimes.Clock;
import ir.markazandroid.widget.calendar.sources.com.github.praytimes.Coordinate;
import ir.markazandroid.widget.calendar.sources.com.github.praytimes.PrayTime;
import ir.markazandroid.widget.calendar.sources.com.github.praytimes.PrayTimesCalculator;

import static android.content.ContentValues.TAG;

public class CalendarFragment extends FrameLayout implements View.OnClickListener {

    private UnitecCalanader unitecCalanader;
    private MonthFragment fragment;

    public UnitecCalanader getunitecCalander() {
        return unitecCalanader;
    }

    public CalendarFragment(Context context, UnitecCalanader unitecCalander) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(context);

        if (context == null) return;

        /// setHasOptionsMenu(true);
        this.unitecCalanader = unitecCalander;

        mainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar, this,
                false);
        viewPagerPosition = 0;
        addView(mainBinding.getRoot());
        boolean isRTL = UIUtils.isRTL(context);
        List<String> titles = new ArrayList<>();
        List<View> tabs = new ArrayList<>();

        titles.add(getContext().getString(R.string.calendar));
        calendarsBinding = DataBindingUtil.inflate(inflater, R.layout.calendars_tab_content, this, false);
        tabs.add(calendarsBinding.getRoot());
        //calendarsBinding.getRoot().setRotationX(180);

        titles.add(getContext().getString(R.string.events));
        eventsBinding = DataBindingUtil.inflate(inflater, R.layout.events_tab_content, this, false);
        tabs.add(eventsBinding.getRoot());
        // eventsBinding.getRoot().setRotationX(180);

        coordinate = Utils.getCoordinate(context);
        if (coordinate != null && Utils.sayAthan == true) {
            titles.add(getContext().getString(R.string.owghat));
            owghatBinding = DataBindingUtil.inflate(inflater, R.layout.owghat_tab_content, this, false);
            tabs.add(owghatBinding.getRoot());
            owghatBinding.getRoot().setOnClickListener(this);
            mainBinding.viewstack.addView(owghatBinding.getRoot());
            // owghatBinding.getRoot().setRotationX(180);
        }

        //this.flipper=(ViewFlipper)findViewById(R.id.flipper);
        /// mainBinding.cardsViewPager.setAdapter(new CardTabsAdapter(getChildFragmentManager(), tabs));
        mainBinding.viewstack.addView(calendarsBinding.getRoot());
        mainBinding.viewstack.addView(eventsBinding.getRoot());
        //todo it must be revised
        //mainBinding.tabLayout.setupWithViewPager(mainBinding.cardsViewPager);
        mainBinding.viewstack.addOnLayoutChangeListener(onLayoutChangeListener_viewFlipper);
        //mainBinding.tabLayout.addTab();
        mainBinding.viewstack.setAutoStart(true);
        mainBinding.viewstack.setFlipInterval(5000);
        mainBinding.viewstack.startFlipping();
        //mainBinding.tabLayout.getTabAt(0).setIcon(R.drawable.ic_event);
        //mainBinding.tabLayout.getTabAt(1).setIcon(R.drawable.ic_event_note);
        if (coordinate != null) {
            // mainBinding.tabLayout.getTabAt(2).setIcon(R.drawable.ic_access_time);
        }


        Resources.Theme theme = getContext().getTheme();
        TypedValue value = new TypedValue();
        theme.resolveAttribute(R.attr.colorAccent, value, true);
        int selectedColor = ContextCompat.getColor(context, value.resourceId);
        theme.resolveAttribute(R.attr.colorTextSecond, value, true);
        int unselectedColor = ContextCompat.getColor(context, value.resourceId);
        // https://stackoverflow.com/a/35461201
//        mainBinding.tabLayout.addOnTabSelectedListener(
//                new TabLayout.OnTabSelectedListener()/*ViewPagerOnTabSelectedListener(mainBinding.cardsViewPager)*/ {
//
//                    @Override
//                    public void onTabSelected(TabLayout.Tab tab) {
//                        ////   super.onTabSelected(tab);
//                        tab.getIcon().setColorFilter(selectedColor, PorterDuff.Mode.SRC_IN);
//
//                    }
//
//                    @Override
//                    public void onTabUnselected(TabLayout.Tab tab) {
//                        ////super.onTabUnselected(tab);
//                        tab.getIcon().setColorFilter(unselectedColor, PorterDuff.Mode.SRC_IN);
//                    }
//
//                    @Override
//                    public void onTabReselected(TabLayout.Tab tab) {
//                        ////   super.onTabReselected(tab);
//                    }
//                }
//        );
        // https://stackoverflow.com/a/49455239 but obviously a hack we will try to remove
        if (isRTL) {
            for (View tab : tabs) {
                ///tab.setRotationY(180);
            }
            ///mainBinding.cardsViewPager.setRotationY(180);
            // mainBinding.viewstack.setRotationY(180);
        }
//        timer = new Timer();
//
//        TimerTask autoTabChanger = new AutoTabChanger();
//        timer.scheduleAtFixedRate(autoTabChanger, 0, 5000);
        prayTimesCalculator = new PrayTimesCalculator(Utils.getCalculationMethod());
        //
        // mainBinding.calendarViewPager.setAdapter(new CalendarAdapter(getChildFragmentManager(), isRTL));
        fragment = new MonthFragment(getContext(), this, 0);
        //fragment.setArguments(0);
        mainBinding.MounthFragment.addView(fragment);
        //CalendarAdapter.gotoOffset(mainBinding.calendarViewPager, 0);

        //mainBinding.calendarViewPager.addOnPageChangeListener(changeListener);

        calendarsBinding.today.setVisibility(View.GONE);
        calendarsBinding.todayIcon.setVisibility(View.GONE);
        calendarsBinding.today.setOnClickListener(this);
        calendarsBinding.todayIcon.setOnClickListener(this);

        calendarsBinding.firstCalendarDateLinear.setOnClickListener(this);
        calendarsBinding.firstCalendarDateDay.setOnClickListener(this);
        calendarsBinding.firstCalendarDate.setOnClickListener(this);
        calendarsBinding.secondCalendarDateLinear.setOnClickListener(this);
        calendarsBinding.secondCalendarDateDay.setOnClickListener(this);
        calendarsBinding.secondCalendarDate.setOnClickListener(this);
        calendarsBinding.thirdCalendarDateLinear.setOnClickListener(this);
        calendarsBinding.thirdCalendarDateDay.setOnClickListener(this);
        calendarsBinding.thirdCalendarDate.setOnClickListener(this);

        calendarsBinding.getRoot().setOnClickListener(this);

        calendarsBinding.firstCalendarDateLinear.setVisibility(View.VISIBLE);
        calendarsBinding.secondCalendarDateLinear.setVisibility(View.VISIBLE);
        calendarsBinding.thirdCalendarDateLinear.setVisibility(View.VISIBLE);
        calendarsBinding.diffDateContainer.setVisibility(View.VISIBLE);
        calendarsBinding.moreCalendar.setVisibility(View.GONE);

        String cityName = Utils.getCityName(context, false);
        if (!TextUtils.isEmpty(cityName) && Utils.sayAthan == true && coordinate != null) {
            owghatBinding.owghatText.setText(cityName);
        }

        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int lastTab = Constants.CALENDARS_TAB;//prefs.getInt(Constants.LAST_CHOSEN_TAB_KEY, Constants.CALENDARS_TAB);
        if (lastTab >= tabs.size()) {
            lastTab = Constants.CALENDARS_TAB;
        }
        /// mainBinding.cardsViewPager.setCurrentItem(lastTab, false);
//        mainBinding.tabLayout.getTabAt(lastTab).getIcon().setColorFilter(selectedColor,
//                PorterDuff.Mode.SRC_IN);
        AbstractDate today = CalendarUtils.getTodayOfCalendar(Utils.getMainCalendar());
//        UIUtils.setActivityTitleAndSubtitle(getActivity(), CalendarUtils.getMonthName(today),
        unitecCalander.setTitle(CalendarUtils.getMonthName(today));
        unitecCalander.setsubTitle(Utils.formatNumber(today.getYear()));
//                Utils.formatNumber(today.getYear()));
        //toolbar insert text view in it

        // Easter egg to test AthanActivity
        if (coordinate != null && Utils.sayAthan == true) {
            owghatBinding.owghatText.setOnClickListener(this);
            owghatBinding.owghatText.setOnLongClickListener(v -> {
                Utils.startAthan(context, "FAJR");
                return true;
            });
        }

        //addView(mainBinding.getRoot());

        //return mainBinding.getRoot();

    }

    //    class AutoTabChanger extends TimerTask {
//
//
//        public void run() {
//            Handler handler = new Handler(getContext().getMainLooper());
//         handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    TabLayout.Tab tab = mainBinding.tabLayout.getTabAt(
//                            (mainBinding.tabLayout.getSelectedTabPosition() + 1) % mainBinding.tabLayout.getTabCount());
//                    tab.select();
//                }
//            });
//
//        }
//    }
    View.OnLayoutChangeListener onLayoutChangeListener_viewFlipper = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
            if (owghatBinding != null && Utils.sayAthan) {
                if (mainBinding.viewstack.getCurrentView() == owghatBinding.getRoot()) {
                    View sunView = owghatBinding.getRoot().findViewById(R.id.svPlot);
                    if (sunView != null && sunView instanceof SunView) {
                        SunView sun = (SunView) sunView;

                        sun.startAnimate(false);

                        // sun.clear();

                    }


                }
            } else if (mainBinding.viewstack.getCurrentView() == eventsBinding.getRoot())
                Log.d("test", "eventsBinding");
            else if (mainBinding.viewstack.getCurrentView() == calendarsBinding.getRoot())
                Log.d("test", "calendarsBinding");


        }
    };

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);


    }


    /**
     * this method diallocate al resurces that allocated durng runing process
     */
    public void dispose() {
        fragment.dispose();
        mainBinding.viewstack.stopFlipping();
    }

    private Calendar calendar = Calendar.getInstance();
    private Coordinate coordinate;
    private PrayTimesCalculator prayTimesCalculator;
    private int viewPagerPosition;
    private FragmentCalendarBinding mainBinding;
    private CalendarsTabContentBinding calendarsBinding;
    private OwghatTabContentBinding owghatBinding;
    private EventsTabContentBinding eventsBinding;
    //private Timer timer;

//    @Nullable
//    @Override
//    public View onCreateView(
//            LayoutInflater inflater,
//            @Nullable ViewGroup container,
//            @Nullable Bundle savedInstanceState) {
//
//        Context context = getContext();
//        if (context == null) return null;
//
//        /// setHasOptionsMenu(true);
//
//        mainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_calendar, container,
//                false);
//        viewPagerPosition = 0;
//
//        boolean isRTL = UIUtils.isRTL(context);
//        List<String> titles = new ArrayList<>();
//        List<View> tabs = new ArrayList<>();
//
//        titles.add(getContext().getString(R.string.calendar));
//        calendarsBinding = DataBindingUtil.inflate(inflater, R.layout.calendars_tab_content, container, false);
//        tabs.add(calendarsBinding.getRoot());
//
//        titles.add(getContext().getString(R.string.events));
//        eventsBinding = DataBindingUtil.inflate(inflater, R.layout.events_tab_content, container, false);
//        tabs.add(eventsBinding.getRoot());
//
//        coordinate = Utils.getCoordinate(context);
//        if (coordinate != null) {
//            titles.add(getContext().getString(R.string.owghat));
//            owghatBinding = DataBindingUtil.inflate(inflater, R.layout.owghat_tab_content, container, false);
//            tabs.add(owghatBinding.getRoot());
//            owghatBinding.getRoot().setOnClickListener(this);
//        }
//
//        mainBinding.cardsViewPager.setAdapter(new CardTabsAdapter(getChildFragmentManager(), tabs));
//        mainBinding.tabLayout.setupWithViewPager(mainBinding.cardsViewPager);
//
//        mainBinding.tabLayout.getTabAt(0).setIcon(R.drawable.ic_event);
//        mainBinding.tabLayout.getTabAt(1).setIcon(R.drawable.ic_event_note);
//        if (coordinate != null) {
//            mainBinding.tabLayout.getTabAt(2).setIcon(R.drawable.ic_access_time);
//        }
//
//        Resources.Theme theme = context.getTheme();
//        TypedValue value = new TypedValue();
//
//        theme.resolveAttribute(R.attr.colorAccent, value, true);
//        int selectedColor = ContextCompat.getColor(context, value.resourceId);
//
//        theme.resolveAttribute(R.attr.colorTextSecond, value, true);
//        int unselectedColor = ContextCompat.getColor(context, value.resourceId);
//
//        // https://stackoverflow.com/a/35461201
//        mainBinding.tabLayout.addOnTabSelectedListener(
//                new TabLayout.ViewPagerOnTabSelectedListener(mainBinding.cardsViewPager) {
//
//                    @Override
//                    public void onTabSelected(TabLayout.Tab tab) {
//                        super.onTabSelected(tab);
//                        tab.getIcon().setColorFilter(selectedColor, PorterDuff.Mode.SRC_IN);
//                    }
//
//                    @Override
//                    public void onTabUnselected(TabLayout.Tab tab) {
//                        super.onTabUnselected(tab);
//                        tab.getIcon().setColorFilter(unselectedColor, PorterDuff.Mode.SRC_IN);
//                    }
//
//                    @Override
//                    public void onTabReselected(TabLayout.Tab tab) {
//                        super.onTabReselected(tab);
//                    }
//                }
//        );
//
//        // https://stackoverflow.com/a/49455239 but obviously a hack we will try to remove
//        if (isRTL) {
//            for (View tab : tabs) {
//                tab.setRotationY(180);
//            }
//            mainBinding.cardsViewPager.setRotationY(180);
//        }
//        timer = new Timer();
//
//        TimerTask autoTabChanger = new AutoTabChanger();
//        timer.scheduleAtFixedRate(autoTabChanger, 0, 5000);
//        prayTimesCalculator = new PrayTimesCalculator(Utils.getCalculationMethod());
//        mainBinding.calendarViewPager.setAdapter(new CalendarAdapter(getChildFragmentManager(), isRTL));
//        CalendarAdapter.gotoOffset(mainBinding.calendarViewPager, 0);
//
//        mainBinding.calendarViewPager.addOnPageChangeListener(changeListener);
//
//        calendarsBinding.today.setVisibility(View.GONE);
//        calendarsBinding.todayIcon.setVisibility(View.GONE);
//        calendarsBinding.today.setOnClickListener(this);
//        calendarsBinding.todayIcon.setOnClickListener(this);
//
//        calendarsBinding.firstCalendarDateLinear.setOnClickListener(this);
//        calendarsBinding.firstCalendarDateDay.setOnClickListener(this);
//        calendarsBinding.firstCalendarDate.setOnClickListener(this);
//        calendarsBinding.secondCalendarDateLinear.setOnClickListener(this);
//        calendarsBinding.secondCalendarDateDay.setOnClickListener(this);
//        calendarsBinding.secondCalendarDate.setOnClickListener(this);
//        calendarsBinding.thirdCalendarDateLinear.setOnClickListener(this);
//        calendarsBinding.thirdCalendarDateDay.setOnClickListener(this);
//        calendarsBinding.thirdCalendarDate.setOnClickListener(this);
//
//        calendarsBinding.getRoot().setOnClickListener(this);
//
//        calendarsBinding.firstCalendarDateLinear.setVisibility(View.VISIBLE);
//        calendarsBinding.secondCalendarDateLinear.setVisibility(View.VISIBLE);
//        calendarsBinding.thirdCalendarDateLinear.setVisibility(View.VISIBLE);
//        calendarsBinding.diffDateContainer.setVisibility(View.VISIBLE);
//        calendarsBinding.moreCalendar.setVisibility(View.GONE);
//
//        String cityName = Utils.getCityName(context, false);
//        if (!TextUtils.isEmpty(cityName)) {
//            owghatBinding.owghatText.setText(cityName);
//        }
//
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//        int lastTab = prefs.getInt(Constants.LAST_CHOSEN_TAB_KEY, Constants.CALENDARS_TAB);
//        if (lastTab >= tabs.size()) {
//            lastTab = Constants.CALENDARS_TAB;
//        }
//
//        mainBinding.cardsViewPager.setCurrentItem(lastTab, false);
//        mainBinding.tabLayout.getTabAt(lastTab).getIcon().setColorFilter(selectedColor,
//                PorterDuff.Mode.SRC_IN);
//
//        AbstractDate today = CalendarUtils.getTodayOfCalendar(Utils.getMainCalendar());
////        UIUtils.setActivityTitleAndSubtitle(getActivity(), CalendarUtils.getMonthName(today),
////                Utils.formatNumber(today.getYear()));
//        //toolbar insert text view in it
//
//        // Easter egg to test AthanActivity
//        if (coordinate != null) {
//            owghatBinding.owghatText.setOnClickListener(this);
//            owghatBinding.owghatText.setOnLongClickListener(v -> {
//                Utils.startAthan(context, "FAJR");
//                return true;
//            });
//        }
//
//        return mainBinding.getRoot();
//
//    }

    public boolean firstTime = true;

    ViewPager.OnPageChangeListener changeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            Context context = getContext();
            if (context == null) return;

            LocalBroadcastManager.getInstance(context).sendBroadcast(
                    new Intent(Constants.BROADCAST_INTENT_TO_MONTH_FRAGMENT)
                            .putExtra(Constants.BROADCAST_FIELD_TO_MONTH_FRAGMENT,
                                    CalendarAdapter.positionToOffset(position))
                            .putExtra(Constants.BROADCAST_FIELD_SELECT_DAY_JDN, lastSelectedJdn));
            Log.e(TAG, "  calendarsBinding.today.setVisibility(View.VISIBLE); ");
            calendarsBinding.today.setVisibility(View.VISIBLE);
            calendarsBinding.todayIcon.setVisibility(View.VISIBLE);
            Log.e(TAG, "after   calendarsBinding.today.setVisibility(View.VISIBLE); ");
        }

    };

    void changeMonth(int position) {
//        mainBinding.calendarViewPager.setCurrentItem(
//                mainBinding.calendarViewPager.getCurrentItem() + position, true);


    }

    private long lastSelectedJdn = -1;

    public void selectDay(long jdn) {
        Context context = getContext();
        if (context == null) return;
        lastSelectedJdn = jdn;
        UIUtils.fillCalendarsCard(context, jdn, calendarsBinding, Utils.getMainCalendar(),
                Utils.getEnabledCalendarTypes());
        boolean isToday = CalendarUtils.getTodayJdn() == jdn;
        if (Utils.sayAthan && Utils.coordinate != null) {
            setOwghat(jdn, isToday);
        }
        showEvent(jdn);
    }

//    public void addEventOnCalendar(long jdn) {
//        Activity activity = getActivity();
//        if (activity == null) return;
//
//        CivilDate civil = DateConverter.jdnToCivil(jdn);
//        Calendar time = Calendar.getInstance();
//        time.set(civil.getYear(), civil.getMonth() - 1, civil.getDayOfMonth());
//
//        try {
//            startActivityForResult(
//                    new Intent(Intent.ACTION_INSERT)
//                            .setData(CalendarContract.Events.CONTENT_URI)
//                            .putExtra(CalendarContract.Events.DESCRIPTION, CalendarUtils.dayTitleSummary(
//                                    CalendarUtils.getDateFromJdnOfCalendar(Utils.getMainCalendar(), jdn)))
//                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
//                                    time.getTimeInMillis())
//                            .putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
//                                    time.getTimeInMillis())
//                            .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true),
//                    CALENDAR_EVENT_ADD_MODIFY_REQUEST_CODE);
//        } catch (Exception e) {
//            Toast.makeText(activity, R.string.device_calendar_does_not_support, Toast.LENGTH_SHORT).show();
//        }
//    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        MainActivity activity = (MainActivity) getActivity();
//        if (activity == null) return;
//
//        if (requestCode == CALENDAR_EVENT_ADD_MODIFY_REQUEST_CODE) {
//            if (Utils.isShowDeviceCalendarEvents()) {
//                LocalBroadcastManager.getInstance(activity).sendBroadcast(
//                        new Intent(Constants.BROADCAST_INTENT_TO_MONTH_FRAGMENT)
//                                .putExtra(Constants.BROADCAST_FIELD_TO_MONTH_FRAGMENT, viewPagerPosition)
//                                .putExtra(Constants.BROADCAST_FIELD_EVENT_ADD_MODIFY, true)
//                                .putExtra(Constants.BROADCAST_FIELD_SELECT_DAY_JDN, lastSelectedJdn));
////            } else {
////                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR)
////                        != PackageManager.PERMISSION_GRANTED) {
////                    UIUtils.askForCalendarPermission(activity);
////                } else {
////                    UIUtils.toggleShowCalendarOnPreference(activity, true);
////                    /////activity.restartActivity();
////                }
//            }
//        }
//    }

    private SpannableString formatClickableEventTitle(DeviceCalendarEvent event) {
        Context context = getContext();
        if (context == null) return null;

        String title = UIUtils.formatDeviceCalendarEventTitle(event);
        SpannableString ss = new SpannableString(title);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                try {
//                    startActivityForResult(new Intent(Intent.ACTION_VIEW)
//                                    .setData(ContentUris.withAppendedId(
//                                            CalendarContract.Events.CONTENT_URI, event.getId())),
//                            CALENDAR_EVENT_ADD_MODIFY_REQUEST_CODE);
                } catch (Exception e) { // Should be ActivityNotFoundException but we don't care really
                    Toast.makeText(context, R.string.device_calendar_does_not_support, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                String color = event.getColor();
                if (!TextUtils.isEmpty(color)) {
                    try {
                        ds.setColor(Integer.parseInt(color));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        ss.setSpan(clickableSpan, 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;
    }

    private SpannableStringBuilder getDeviceEventsTitle(List<AbstractEvent> dayEvents) {
        SpannableStringBuilder titles = new SpannableStringBuilder();
        boolean first = true;

        for (AbstractEvent event : dayEvents)
            if (event instanceof DeviceCalendarEvent) {
                if (first)
                    first = false;
                else
                    titles.append("\n");

                titles.append(formatClickableEventTitle((DeviceCalendarEvent) event));
            }

        return titles;
    }

    private void showEvent(long jdn) {
        List<AbstractEvent> events = Utils.getEvents(jdn,
                CalendarUtils.readDayDeviceEvents(getContext(), jdn));
        String holidays = Utils.getEventsTitle(events, true, false, false, false);
        String nonHolidays = Utils.getEventsTitle(events, false, false, false, false);
        SpannableStringBuilder deviceEvents = getDeviceEventsTitle(events);

        eventsBinding.holidayTitle.setVisibility(View.GONE);
        eventsBinding.deviceEventTitle.setVisibility(View.GONE);
        eventsBinding.eventTitle.setVisibility(View.GONE);
        eventsBinding.eventMessage.setVisibility(View.GONE);
        eventsBinding.noEvent.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(holidays)) {
            eventsBinding.noEvent.setVisibility(View.GONE);
            eventsBinding.holidayTitle.setText(holidays);
            eventsBinding.holidayTitle.setVisibility(View.VISIBLE);
        }

        if (deviceEvents.length() != 0) {
            eventsBinding.noEvent.setVisibility(View.GONE);
            eventsBinding.deviceEventTitle.setText(deviceEvents);
            eventsBinding.deviceEventTitle.setMovementMethod(LinkMovementMethod.getInstance());

            eventsBinding.deviceEventTitle.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(nonHolidays)) {
            eventsBinding.noEvent.setVisibility(View.GONE);
            eventsBinding.eventTitle.setText(nonHolidays);

            eventsBinding.eventTitle.setVisibility(View.VISIBLE);
        }

        SpannableStringBuilder messageToShow = new SpannableStringBuilder();

        Context context = getContext();
        if (context == null) return;

//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
//        Set<String> enabledTypes = prefs.getStringSet(PREF_HOLIDAY_TYPES, new HashSet<>());

        Set<String> enabledTypes = new HashSet<>(); /*prefs.getStringSet(PREF_HOLIDAY_TYPES, new HashSet<>())*/
        enabledTypes.add("iran_holidays");
        enabledTypes.add("afghanistan_holidays");
        enabledTypes.add("afghanistan_others");
        enabledTypes.add("iran_holidays");
        enabledTypes.add("iran_islamic");
        enabledTypes.add("iran_ancient");
        enabledTypes.add("iran_others");
        enabledTypes.add("international");


        if (enabledTypes.size() == 0) {
            eventsBinding.noEvent.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(messageToShow))
                messageToShow.append("\n");

            String title = getContext().getString(R.string.warn_if_events_not_set);
            SpannableString ss = new SpannableString(title);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    // ((MainActivity) getActivity()).bringPreferences();
                }
            };
            ss.setSpan(clickableSpan, 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            messageToShow.append(ss);
        }

        if (!TextUtils.isEmpty(messageToShow)) {
            eventsBinding.eventMessage.setText(messageToShow);
            eventsBinding.eventMessage.setMovementMethod(LinkMovementMethod.getInstance());

            eventsBinding.eventMessage.setVisibility(View.VISIBLE);
        }
    }

    private void setOwghat(long jdn, boolean isToday) {
        if (coordinate == null || Utils.sayAthan == false) {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        CivilDate civilDate = DateConverter.jdnToCivil(jdn);
        calendar.set(civilDate.getYear(), civilDate.getMonth() - 1, civilDate.getDayOfMonth());
        Date date = calendar.getTime();

        Map<PrayTime, Clock> prayTimes = prayTimesCalculator.calculate(date, coordinate);

        owghatBinding.imsak.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.IMSAK)));
        Clock sunriseClock = prayTimes.get(PrayTime.FAJR);
        owghatBinding.fajr.setText(UIUtils.getFormattedClock(sunriseClock));
        owghatBinding.sunrise.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.SUNRISE)));
        Clock midddayClock = prayTimes.get(PrayTime.DHUHR);
        owghatBinding.dhuhr.setText(UIUtils.getFormattedClock(midddayClock));
        owghatBinding.asr.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.ASR)));
        owghatBinding.sunset.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.SUNSET)));
        Clock maghribClock = prayTimes.get(PrayTime.MAGHRIB);
        owghatBinding.maghrib.setText(UIUtils.getFormattedClock(maghribClock));
        owghatBinding.isgha.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.ISHA)));
        owghatBinding.midnight.setText(UIUtils.getFormattedClock(prayTimes.get(PrayTime.MIDNIGHT)));

        double moonPhase = 1;
        try {
            moonPhase = new SunMoonPosition(CalendarUtils.getTodayJdn(), coordinate.getLatitude(),
                    coordinate.getLongitude(), 0, 0).getMoonPhase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        owghatBinding.svPlot.setSunriseSunsetMoonPhase(prayTimes, moonPhase);

        if (isToday) {
            owghatBinding.svPlot.setVisibility(View.VISIBLE);
            if (mainBinding.viewstack.getCurrentView() == owghatBinding.getRoot()) {
                owghatBinding.svPlot.startAnimate(true);
            }
        } else {
            owghatBinding.svPlot.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Context context = getContext();
        if (context == null) return;

        int id = v.getId();//COMENTED b tahami do to extra information that have ben shown
//            case R.id.calendars_tab_content:
//                boolean isOpenCalendarCommand = calendarsBinding.firstCalendarDateLinear.getVisibility() != View.VISIBLE;
//
//                calendarsBinding.moreCalendar.setImageResource(isOpenCalendarCommand
//                        ? R.drawable.ic_keyboard_arrow_up
//                        : R.drawable.ic_keyboard_arrow_down);
//                calendarsBinding.firstCalendarDateLinear.setVisibility(isOpenCalendarCommand ? View.VISIBLE : View.GONE);
//                calendarsBinding.secondCalendarDateLinear.setVisibility(isOpenCalendarCommand ? View.VISIBLE : View.GONE);
//                calendarsBinding.thirdCalendarDateLinear.setVisibility(isOpenCalendarCommand ? View.VISIBLE : View.GONE);
//                calendarsBinding.diffDateContainer.setVisibility(isOpenCalendarCommand ? View.VISIBLE : View.GONE);
//
//                break;
        if (id == R.id.owghat_text || id == R.id.owghat_content) {
            boolean isOpenOwghatCommand = owghatBinding.sunriseLayout.getVisibility() == View.GONE;

            owghatBinding.moreOwghat.setImageResource(isOpenOwghatCommand
                    ? R.drawable.ic_keyboard_arrow_up
                    : R.drawable.ic_keyboard_arrow_down);
            owghatBinding.imsakLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
            owghatBinding.sunriseLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
            owghatBinding.asrLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
            owghatBinding.sunsetLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
            owghatBinding.ishaLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
            owghatBinding.midnightLayout.setVisibility(isOpenOwghatCommand ? View.VISIBLE : View.GONE);
//todo this must be revized
            //////   mainBinding.cardsViewPager.measureCurrentView(owghatBinding.getRoot());

            if (lastSelectedJdn == -1)
                lastSelectedJdn = CalendarUtils.getTodayJdn();
        } else if (id == R.id.today || id == R.id.today_icon) {
            bringTodayYearMonth();
        } else if (id == R.id.first_calendar_date || id == R.id.first_calendar_date_day || id == R.id.first_calendar_date_linear || id == R.id.second_calendar_date || id == R.id.second_calendar_date_day || id == R.id.second_calendar_date_linear || id == R.id.third_calendar_date || id == R.id.third_calendar_date_day || id == R.id.third_calendar_date_linear) {//                UIUtils.copyToClipboard(context, calendarsBinding.firstCalendarDateDay.getText() + " " +
//                        calendarsBinding.firstCalendarDate.getText().toString().replace("\n", " "));
//                break;


//                UIUtils.copyToClipboard(context, calendarsBinding.firstCalendarDateLinear.getText());
//                break;


//                UIUtils.copyToClipboard(context, calendarsBinding.secondCalendarDateDay.getText() + " " +
//                        calendarsBinding.secondCalendarDate.getText().toString().replace("\n", " "));
//                break;


//                UIUtils.copyToClipboard(context, calendarsBinding.secondCalendarDateLinear.getText());
//                break;


//                UIUtils.copyToClipboard(context, calendarsBinding.thirdCalendarDateDay.getText() + " " +
//                        calendarsBinding.thirdCalendarDate.getText().toString().replace("\n", " "));
//                break;


//                UIUtils.copyToClipboard(context, calendarsBinding.thirdCalendarDateLinear.getText());
//                break;
        }
    }

    public void bringTodayYearMonth() {
        Context context = getContext();
        if (context == null) return;
        Log.e(TAG, "bringTodayYearMonth: ");
        lastSelectedJdn = -1;
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(Constants.BROADCAST_INTENT_TO_MONTH_FRAGMENT)
                        .putExtra(Constants.BROADCAST_FIELD_TO_MONTH_FRAGMENT,
                                Constants.BROADCAST_TO_MONTH_FRAGMENT_RESET_DAY)
                        .putExtra(Constants.BROADCAST_FIELD_SELECT_DAY_JDN, -1));

        //CalendarAdapter.gotoOffset(mainBinding.calendarViewPager, 0);

        selectDay(CalendarUtils.getTodayJdn());

    }

    public void bringDate(long jdn) {
        Context context = getContext();
        if (context == null) return;

        CalendarType mainCalendar = Utils.getMainCalendar();
        AbstractDate today = CalendarUtils.getTodayOfCalendar(mainCalendar);
        AbstractDate date = CalendarUtils.getDateFromJdnOfCalendar(mainCalendar, jdn);
        viewPagerPosition =
                (today.getYear() - date.getYear()) * 12 + today.getMonth() - date.getMonth();
        //CalendarAdapter.gotoOffset(mainBinding.calendarViewPager, viewPagerPosition);

        selectDay(jdn);

        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(Constants.BROADCAST_INTENT_TO_MONTH_FRAGMENT)
                        .putExtra(Constants.BROADCAST_FIELD_TO_MONTH_FRAGMENT, viewPagerPosition)
                        .putExtra(Constants.BROADCAST_FIELD_SELECT_DAY_JDN, jdn));
    }

//    private SearchView mSearchView;
//    private SearchView.SearchAutoComplete mSearchAutoComplete;
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        menu.clear();
//        inflater.inflate(R.menu.calendar_menu_buttons, menu);
//
//        mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();
//        mSearchView.setOnSearchClickListener(v -> {
//            if (mSearchAutoComplete != null) mSearchAutoComplete.setOnItemClickListener(null);
//
//            Context context = getContext();
//            if (context == null) return;
//
//            mSearchAutoComplete = mSearchView.findViewById(androidx.appcompat.R.id.search_src_text);
//            mSearchAutoComplete.setHint(R.string.search_in_events);
//
//            ArrayAdapter<AbstractEvent> eventsAdapter = new ArrayAdapter<>(context,
//                    R.layout.suggestion, android.R.id.text1);
//            eventsAdapter.addAll(Utils.getAllEnabledEvents());
//            eventsAdapter.addAll(CalendarUtils.getAllEnabledAppointments(context));
//            mSearchAutoComplete.setAdapter(eventsAdapter);
//            mSearchAutoComplete.setOnItemClickListener((parent, view, position, id) -> {
//                AbstractEvent ev = (AbstractEvent) parent.getItemAtPosition(position);
//                if (ev instanceof PersianCalendarEvent) {
//                    PersianDate todayPersian = CalendarUtils.getPersianToday();
//                    PersianDate date = ((PersianCalendarEvent) ev).getDate();
//                    int year = date.getYear();
//                    if (year == -1) {
//                        year = todayPersian.getYear() +
//                                (date.getMonth() < todayPersian.getMonth() ? 1 : 0);
//                    }
//                    bringDate(DateConverter.persianToJdn(year, date.getMonth(), date.getDayOfMonth()));
//                } else if (ev instanceof IslamicCalendarEvent) {
//                    IslamicDate todayIslamic = CalendarUtils.getIslamicToday();
//                    IslamicDate date = ((IslamicCalendarEvent) ev).getDate();
//                    int year = date.getYear();
//                    if (year == -1) {
//                        year = todayIslamic.getYear() +
//                                (date.getMonth() < todayIslamic.getMonth() ? 1 : 0);
//                    }
//                    bringDate(DateConverter.islamicToJdn(year, date.getMonth(), date.getDayOfMonth()));
//                } else if (ev instanceof GregorianCalendarEvent) {
//                    CivilDate todayCivil = CalendarUtils.getGregorianToday();
//                    CivilDate date = ((GregorianCalendarEvent) ev).getDate();
//                    int year = date.getYear();
//                    if (year == -1) {
//                        year = todayCivil.getYear() +
//                                (date.getMonth() < todayCivil.getMonth() ? 1 : 0);
//                    }
//                    bringDate(DateConverter.civilToJdn(year, date.getMonth(), date.getDayOfMonth()));
//                } else if (ev instanceof DeviceCalendarEvent) {
//                    CivilDate todayCivil = CalendarUtils.getGregorianToday();
//                    CivilDate date = ((DeviceCalendarEvent) ev).getCivilDate();
//                    int year = date.getYear();
//                    if (year == -1) {
//                        year = todayCivil.getYear() +
//                                (date.getMonth() < todayCivil.getMonth() ? 1 : 0);
//                    }
//                    bringDate(DateConverter.civilToJdn(year, date.getMonth(), date.getDayOfMonth()));
//                }
//                mSearchView.onActionViewCollapsed();
//            });
//        });
//    }

//    private void destroySearchView() {
//        if (mSearchView != null) {
//            mSearchView.setOnSearchClickListener(null);
//            mSearchView = null;
//        }
//
//        if (mSearchAutoComplete != null) {
//            mSearchAutoComplete.setAdapter(null);
//            mSearchAutoComplete.setOnItemClickListener(null);
//            mSearchAutoComplete = null;
//        }
//    }

//    @Override
//    public void onDestroyOptionsMenu() {
//        destroySearchView();
//        super.onDestroyOptionsMenu();
//    }

//    @Override
//    public void onDestroy() {
//       // destroySearchView();
//        timer.cancel();
//        super.onDestroy();
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.go_to:
//                new SelectDayDialog(lastSelectedJdn).show(getChildFragmentManager(),
//                        SelectDayDialog.class.getName());
//                break;
//            case R.id.today:
//                bringTodayYearMonth();
//                break;
//            case R.id.add_event:
//                if (lastSelectedJdn == -1)
//                    lastSelectedJdn = CalendarUtils.getTodayJdn();
//
//             //   addEventOnCalendar(lastSelectedJdn);
//                break;
//            default:
//                break;
//        }
//        return true;
//    }

    public int getViewPagerPosition() {
        return viewPagerPosition;
    }

//    public boolean closeSearch() {
//        if (mSearchView != null && !mSearchView.isIconified()) {
//            mSearchView.onActionViewCollapsed();
//            return true;
//        }
//        return false;
//    }

}
