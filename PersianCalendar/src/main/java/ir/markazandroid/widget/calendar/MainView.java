package ir.markazandroid.widget.calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;

import com.byagowi.persiancalendar.R;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import ir.markazandroid.widget.Widget;
import ir.markazandroid.widget.calendar.sources.calendar.AbstractDate;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.Constants;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.CalendarUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.TypeFaceUtil;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UpdateUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity.UnitecAzanVeiw;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity.UnitecCalanader;

import static android.content.ContentValues.TAG;

/**
 * Created by Damian on 22/04/2017.
 */
public class MainView extends RelativeLayout implements Widget {

    private BroadcastReceivers broadcastReceivers;
    private UnitecAzanVeiw unitecAzanVeiw;
    private UnitecCalanader unitecCalander;
    private Timer timer = new Timer();

    public MainView(Context context) {
        this(context, null);
    }

    public MainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init(JSONObject extras) {
        Log.e(TAG, "  public void init(JSONObject extras) {: " );
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
//        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
//        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
//        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
//        intentFilter.addAction(BROADCAST_ALARM);
//
//        broadcastReceivers = new BroadcastReceivers();
////        intentFilter.addAction(Intent.ACTION_TIME_TICK);
//        getContext().registerReceiver(broadcastReceivers, intentFilter);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                Handler handler = new Handler(getContext().getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        AbstractDate today = CalendarUtils.getTodayOfCalendar(Utils.getMainCalendar());
//                        unitecCalander.setTitle(CalendarUtils.getMonthName(today));
//                        unitecCalander.setsubTitle(Utils.formatNumber(today.getYear()));
                        removeView(unitecCalander);
                        removeView(unitecAzanVeiw);
                        // unitecAzanVeiw.dispose();
                        unitecCalander.dispose();
                        if (Utils.athanTimer != null) {
                            Utils.athanTimer.cancel();
                        }
                        unitecCalander = new UnitecCalanader(getContext(), MainView.this);
                        addView(unitecCalander);
                        Log.e(TAG, "MainVeiw.this.unitecCalander.getCalanderFragment ");
                        //MainVeiw.this.unitecCalander.getCalanderFragment().bringTodayYearMonth();
                        //UpdateUtils.update(MainVeiw.this.getContext(), true, MainVeiw.this);
                    }
                });


            }
        }, calendar.getTime(), 24 * 60 * 60 * 1000L);

//        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        TypeFaceUtil.overrideFont(getContext(), "SERIF", "fonts/NotoNaskhArabic-Regular.ttf");
        Resources.Theme theme = getContext().getTheme();
        TypedValue value = new TypedValue();

        theme.resolveAttribute(R.attr.colorPrimary, value, true);
        Utils.primaryColor = ContextCompat.getColor(getContext(), R.color.light_primary);
        //Utils.primaryColor = getResources().getColor(R.color.light_primary_dark);
        //set the properties for button

        Utils.sayAthan = true;
        Utils.islamicOffset = "0";
        Utils.cityName = "ESFEHAN";
        unitecCalander = new UnitecCalanader(getContext(), this);
        addView(unitecCalander);
        //Utils.setAlarm(getContext(),FAJR,10000,2,0);
        //startAthan(getContext(),"FAJR");
//        FrameLayout o = findViewById(R.id.myyy);
//        o.addView(unitecCalander);
//        Coordinate temp = new Coordinate(10,10,10);
//        Utils.coordinate = temp;
        //Utils.updateStoredPreference(getContext().getApplicationContext());
        Utils.loadApp(getContext());
        //UpdateUtils.update(getContext().getApplicationContext(), true);
//        Button btnTag = new Button(getContext());
//        btnTag.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
//        btnTag.setText("Button");
//        btnTag.bringToFront();
//        addView(btnTag);
//        btnTag.setOnClickListener(new OnClickListener() {
//
//            public void onClick(View v) {
//                if(ISATHANRINGING) {
//                    startAthan(getContext(), "FAJR");
//                    ISATHANRINGING = false;
//                }
//                else
//                {
//                    ISATHANRINGING = true;
//                }
//
//
//            }
//        });
    }

    public void dispose() {
        getContext().unregisterReceiver(broadcastReceivers);
        unitecAzanVeiw.dispose();
        unitecCalander.dispose();
        timer.cancel();
        if (Utils.athanTimer != null) {
            Utils.athanTimer.cancel();
        }
    }

    public void bringUniteCalanderFront() {

        removeView(unitecAzanVeiw);
    }

    public void startAthan(Context context, String prayTimeKey) {

//            context.startActivity(new Intent(context, AthanActivity.class)
//                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                    .putExtra(Constants.KEY_EXTRA_PRAYER_KEY, prayTimeKey));
        unitecAzanVeiw = new UnitecAzanVeiw(context, prayTimeKey, this);
        addView(unitecAzanVeiw);

        unitecAzanVeiw.bringToFront();


    }


    /**
     * Startup broadcast receiver
     *
     * @author ebraminio
     */
    public class BroadcastReceivers extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent == null || intent.getAction() == null) {
                return;
            }

            switch (intent.getAction()) {
                case Intent.ACTION_BOOT_COMPLETED:
                case TelephonyManager.ACTION_PHONE_STATE_CHANGED:
                case Constants.BROADCAST_RESTART_APP:
                    Utils.startEitherServiceOrWorker(context);
                    break;

                case Intent.ACTION_DATE_CHANGED:
                case Intent.ACTION_TIMEZONE_CHANGED:
                    UpdateUtils.update(context, true, MainView.this);
                    Utils.loadApp(context);
                    break;

                case Intent.ACTION_TIME_CHANGED:
                case Intent.ACTION_SCREEN_ON:
                case Constants.BROADCAST_UPDATE_APP:
                    UpdateUtils.update(context, false, MainView.this);
                    Utils.loadApp(context);
                    break;

                case Constants.BROADCAST_ALARM:
                    Log.e(TAG, "onReceive:    case Constants.BROADCAST_ALARM:");
                    startAthan(context, intent.getStringExtra(Constants.KEY_EXTRA_PRAYER_KEY));
                    break;
            }
        }
    }




}