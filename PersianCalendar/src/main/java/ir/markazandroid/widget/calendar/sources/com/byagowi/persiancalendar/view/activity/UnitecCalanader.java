package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.byagowi.persiancalendar.R;
import com.byagowi.persiancalendar.databinding.ActivityCalendarBinding;

import ir.markazandroid.widget.calendar.MainView;
import ir.markazandroid.widget.calendar.sources.calendar.CivilDate;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.CalendarUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UIUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UpdateUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.AboutFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.CalendarFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.CompassFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.ConverterFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.PreferenceFragment;

/**
 * Created by Damian on 22/04/2017.
 */
public class UnitecCalanader extends FrameLayout {

    private TextView title;
    private TextView subtitle;
    private CalendarFragment calendarFragment;

    public CalendarFragment getCalanderFragment() {
        return calendarFragment;
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setsubTitle(String title) {
        subtitle.setText(title);
    }

    private static final int CALENDAR = 0,
            CONVERTER = 1,
            COMPASS = 2,
            PREFERENCE = 3,
            ABOUT = 4,
            EXIT = 5,
            DEFAULT = CALENDAR; // Default selected fragment
    private final String TAG = MainActivity.class.getName();
    private ActivityCalendarBinding binding;

    private final Class<?>[] fragments = {
            CalendarFragment.class,
            ConverterFragment.class,
            CompassFragment.class,
            PreferenceFragment.class,
            AboutFragment.class
    };
    private int menuPosition = -1; // it should be zero otherwise #selectItem won't be called
    private static CivilDate creationDate;

    // https://stackoverflow.com/a/3410200
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public UnitecCalanader(Context context, MainView mainView) {

        this(context, null, mainView);
    }

    public UnitecCalanader(Context context, AttributeSet attrs, MainView mainView) {
        this(context, attrs, 0, mainView);
    }

    public UnitecCalanader(Context context, AttributeSet attrs, int defStyleAttr, MainView mainView) {
        this(context, attrs, defStyleAttr, 0, mainView);
    }


    public UnitecCalanader(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, MainView mainView) {
        super(context, attrs, defStyleAttr, defStyleRes);
        View lof_UnitecCalander = inflate(context, R.layout.activity_calendar, this);
        //addView(lof_UnitecCalander);
        // UIUtils.setTheme(this);
        //Utils.applyAppLanguage(getContext());
        //////requestWindowFeature(Window.FEATURE_NO_TITLE);
        // super.onCreate(savedInstanceState);
        Utils.initUtils(getContext(), mainView);
        // font from assets: "assets/fonts/Roboto-Regular.ttf
        // Utils.startEitherServiceOrWorker(this);
        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.persianCalendar_subtitle);
        // Doesn't matter apparently
        // oneTimeClockDisablingForAndroid5LE();
        UpdateUtils.setDeviceCalendarEvents(getContext().getApplicationContext());
        //UpdateUtils.update(getContext().getApplicationContext(), true,mainVeiw);
        //  binding = DataBindingUtil.setContentView(this, R.layout.activity_calendar);
        ////setSupportActionBar(binding.toolbar);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
//                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//
//            binding.toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
//
//        }
        boolean isRTL = UIUtils.isRTL(getContext());
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Utils.primaryColor);
        creationDate = CalendarUtils.getGregorianToday();
        Utils.applyAppLanguage(getContext());
        FrameLayout lof_Fragment_Holder = findViewById(R.id.fragment_holder);
        calendarFragment = new CalendarFragment(context, this);
        lof_Fragment_Holder.addView(calendarFragment);

    }

    public void dispose() {
        calendarFragment.dispose();
    }


}