package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.Constants;

import com.byagowi.persiancalendar.R;

import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.adapter.MonthAdapter;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.entity.DayEntity;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.CalendarUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UIUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ir.markazandroid.widget.calendar.sources.calendar.AbstractDate;
import ir.markazandroid.widget.calendar.sources.calendar.CalendarType;

public class MonthFragment extends FrameLayout implements View.OnClickListener {
    private AbstractDate typedDate;
    private int offset;
    private MonthAdapter adapter;
    private CalendarFragment calendarFragment;

    public MonthAdapter getAdapter() {
        return adapter;
    }

    public CalendarFragment getCalanderFragment() {
        return calendarFragment;
    }

    private long baseJdn;
    private int monthLength;


    private static boolean isRTL = false;

    //public void setArguments(int offcet) {
    //    this.offset = offcet;
    // }

    public MonthFragment(@NonNull Context context, CalendarFragment fragment, int offset) {

        this(context, null, fragment, offset);

    }

    public MonthFragment(@NonNull Context context, AttributeSet attrs, int defStyleAttr, CalendarFragment fragment, int offset) {
        // super(context, attrs, defStyleAttr);
        this(context, attrs, defStyleAttr, 0, fragment, offset);
    }


    public MonthFragment(@NonNull Context context, AttributeSet attrs, CalendarFragment fragment, int offset) {

        this(context, attrs, 0, fragment, offset);
    }

    public MonthFragment(@NonNull Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, CalendarFragment fragment, int offset) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.calendarFragment = fragment;
        this.offset = offset;

        LayoutInflater lof_layoutInflate = LayoutInflater.from(context);
        View toAdd = onCreateView(lof_layoutInflate, this, null);
        addView(toAdd);
        LinearLayout linearlayout = findViewById(R.id.fragmen_mounth_linear);
        linearlayout.setBackgroundColor(Utils.primaryColor);

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_month, container, false);
        isRTL = UIUtils.isRTL(getContext());
        //   offset = positionToOffset(0);

        // We deliberately like to avoid DataBinding thing here, at least for now
        AppCompatImageView prev = view.findViewById(R.id.prev);
        AppCompatImageView next = view.findViewById(R.id.next);
        prev.setImageResource(isRTL
                ? R.drawable.ic_keyboard_arrow_right
                : R.drawable.ic_keyboard_arrow_left);
        next.setImageResource(isRTL
                ? R.drawable.ic_keyboard_arrow_left
                : R.drawable.ic_keyboard_arrow_right);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);

        RecyclerView recyclerView = view.findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),
                Utils.isWeekOfYearEnabled() ? 8 : 7));
        ///////
        ///////
        ///////
        CalendarType mainCalendar = Utils.getMainCalendar();
        List<DayEntity> days = new ArrayList<>();
        typedDate = CalendarUtils.getTodayOfCalendar(mainCalendar);
        int month = typedDate.getMonth() - offset;
        month -= 1;
        int year = typedDate.getYear();

        year = year + (month / 12);
        month = month % 12;
        if (month < 0) {
            year -= 1;
            month += 12;
        }
        month += 1;
        typedDate = CalendarUtils.getDateOfCalendar(mainCalendar, year, month, 1);

        baseJdn = CalendarUtils.getJdnDate(typedDate);
        monthLength = CalendarUtils.getMonthLength(mainCalendar, year, month);

        int dayOfWeek = CalendarUtils.getDayOfWeekFromJdn(baseJdn);
        long todayJdn = CalendarUtils.getTodayJdn();
        for (int i = 0; i < monthLength; i++) {
            DayEntity dayEntity = new DayEntity();
            dayEntity.setJdn(baseJdn + i);

            if (baseJdn + i == todayJdn) {
                dayEntity.setToday(true);
            }

            dayEntity.setDayOfWeek(dayOfWeek);

            days.add(dayEntity);
            dayOfWeek++;
            if (dayOfWeek == 7) {
                dayOfWeek = 0;
            }
        }
        long startOfYearJdn = CalendarUtils.getJdnOfCalendar(mainCalendar, year, 1, 1);
        int weekOfYearStart = CalendarUtils.calculateWeekOfYear(baseJdn, startOfYearJdn);
        int weeksCount = 1 + CalendarUtils.calculateWeekOfYear(baseJdn + monthLength - 1, startOfYearJdn) - weekOfYearStart;

        int startingDayOfWeek = CalendarUtils.getDayOfWeekFromJdn(baseJdn);
        ///////
        ///////
        ///////
        adapter = new MonthAdapter(getContext(), days, startingDayOfWeek,
                weekOfYearStart, weeksCount, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(null);
        //  calendarFragment = (CalendarFragment) (getParent().getParent());

        if (calendarFragment.firstTime && offset == 0 && calendarFragment.getViewPagerPosition() == offset) {
            calendarFragment.firstTime = false;
            calendarFragment.selectDay(CalendarUtils.getTodayJdn());
            updateTitle();
        }
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(setCurrentMonthReceiver,
                new IntentFilter(Constants.BROADCAST_INTENT_TO_MONTH_FRAGMENT));

        return view;
    }

    private BroadcastReceiver setCurrentMonthReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();

            if (extras == null) return;

            int value = extras.getInt(Constants.BROADCAST_FIELD_TO_MONTH_FRAGMENT);
            if (value == offset) {
                long jdn = extras.getLong(Constants.BROADCAST_FIELD_SELECT_DAY_JDN);

                if (extras.getBoolean(Constants.BROADCAST_FIELD_EVENT_ADD_MODIFY, false)) {
                    adapter.initializeMonthEvents(context);
                    calendarFragment.selectDay(jdn);
                } else {
                    adapter.selectDay(-1);
                    updateTitle();
                }

                long selectedDay = 1 + jdn - baseJdn;
                if (jdn != -1 && jdn >= baseJdn && selectedDay <= monthLength) {
                    adapter.selectDay((int) (1 + jdn - baseJdn));
                }
            } else {
                adapter.selectDay(-1);
            }
        }
    };

    public void dispose() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(setCurrentMonthReceiver);
        adapter.dispose();
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.next:
//                calendarFragment.changeMonth(isRTL ? -1 : 1);
//                break;
//
//            case R.id.prev:
//                calendarFragment.changeMonth(isRTL ? 1 : -1);
//                break;
//        }
    }

    private void updateTitle() {
        calendarFragment.getunitecCalander().setTitle(CalendarUtils.getMonthName(typedDate));
        calendarFragment.getunitecCalander().setsubTitle(Utils.formatNumber(typedDate.getYear()));
        // UIUtils.setActivityTitleAndSubtitle(getActivity(), CalendarUtils.getMonthName(typedDate),
        //            Utils.formatNumber(typedDate.getYear()));
    }
}
