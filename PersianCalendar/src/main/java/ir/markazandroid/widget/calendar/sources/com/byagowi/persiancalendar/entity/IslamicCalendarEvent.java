package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.entity;

import ir.markazandroid.widget.calendar.sources.calendar.IslamicDate;

/**
 * PersianCalendarEvent
 *
 * @author ebraminio
 */
public class IslamicCalendarEvent extends AbstractEvent {
    private IslamicDate date;

    public IslamicCalendarEvent(IslamicDate date, String title, boolean holiday) {
        this.date = date;
        this.title = title;
        this.holiday = holiday;
    }

    public IslamicDate getDate() {
        return date;
    }
}