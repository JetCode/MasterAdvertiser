package ir.markazandroid.widget.calendar.sources.com.github.praytimes;

public enum PrayTime {
    IMSAK, FAJR, SUNRISE, DHUHR, ASR, SUNSET, MAGHRIB, ISHA, MIDNIGHT
}
