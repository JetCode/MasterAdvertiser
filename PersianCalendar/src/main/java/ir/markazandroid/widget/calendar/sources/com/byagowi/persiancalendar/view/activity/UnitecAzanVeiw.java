package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import androidx.appcompat.widget.AppCompatTextView;

import com.byagowi.persiancalendar.R;

import java.io.IOException;

import ir.markazandroid.widget.calendar.MainView;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.UIUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;

/**
 * Created by Damian on 22/04/2017.
 */
public class UnitecAzanVeiw extends FrameLayout implements View.OnClickListener {

    private static final String TAG = AthanActivity.class.getName();
    private Ringtone ringtone;
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    AppCompatTextView lof_AthanName;
    AppCompatTextView lof_Place;
    View root;
    MainView mainView;

    public UnitecAzanVeiw(Context context, String prayTimeKey, MainView mainView) {
        this(context, null, prayTimeKey, mainView);
    }

    public UnitecAzanVeiw(Context context, AttributeSet attrs, String prayTimeKey, MainView mainView) {
        this(context, attrs, 0, prayTimeKey, mainView);
    }

    public UnitecAzanVeiw(Context context, AttributeSet attrs, int defStyleAttr, String prayTimeKey, MainView mainView) {
        this(context, attrs, defStyleAttr, 0, prayTimeKey, mainView);
    }


    public UnitecAzanVeiw(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, String prayTimeKey, MainView mainView) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mainView = mainView;
        root = inflate(context, R.layout.activity_athan, this);
        //addView(root);
        AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, Utils.getAthanVolume(getContext()), 0);
        }

        Uri customAthanUri = Utils.getCustomAthanUri(getContext());
        if (customAthanUri != null) {
            ringtone = RingtoneManager.getRingtone(getContext(), customAthanUri);
            ringtone.setStreamType(AudioManager.STREAM_ALARM);
            ringtone.play();
        } else {
            MediaPlayer player = new MediaPlayer();
            try {
                player.setDataSource(getContext(), UIUtils.getDefaultAthanUri(getContext()));
                player.setAudioStreamType(AudioManager.STREAM_ALARM);
                player.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                player.start();
                mediaPlayer = player;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Utils.applyAppLanguage(getContext());

        //  ActivityAthanBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_athan);
        lof_AthanName = findViewById(R.id.athan_name);
        lof_Place = findViewById(R.id.place);

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
//                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        String prayerKey = prayTimeKey;//getIntent().getStringExtra(Constants.KEY_EXTRA_PRAYER_KEY);
        lof_AthanName.setText(UIUtils.getPrayTimeText(prayerKey));

        //  View root = binding.getRoot();

        root.setOnClickListener(this);
        root.setBackgroundResource(UIUtils.getPrayTimeImage(prayerKey));
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                stop();
            }

        });
        lof_Place.setText(getContext().getString(R.string.in_city_time) + " " + Utils.getCityName(context, true));
        //handler.postDelayed(stopTask, TimeUnit.SECONDS.toMillis(10));

//        try {
//            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
//            if (telephonyManager != null) {
//                telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
//            }
//        } catch (Exception e) {
//            Log.e(TAG, "TelephonyManager handling fail", e);
//        }


    }

//    Runnable stopTask = new Runnable() {
//        @Override
//        public void run() {
//            if (ringtone == null && mediaPlayer == null) {
//                //AthanActivity.this.finish();
//                ((FrameLayout) getParent()).removeView(UnitecAzanVeiw.this);
//
//
//                return;
//            }
//            try {
//                if (ringtone != null) {
//                    if (!ringtone.isPlaying()) {
//                        //   AthanActivity.this.finish();
//                        ((FrameLayout) getParent()).removeView(UnitecAzanVeiw.this);
//                        return;
//                    }
//                }
//                if (mediaPlayer != null) {
//                    if (!mediaPlayer.isPlaying()) {
//                        //AthanActivity.this.finish();
//                        ((RelativeLayout) getParent()).removeView(UnitecAzanVeiw.this);
//                        return;
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                //AthanActivity.this.finish();
//                ((FrameLayout) getParent()).removeView(UnitecAzanVeiw.this);
//                return;
//            }
//            handler.postDelayed(this, TimeUnit.SECONDS.toMillis(50));
//        }
//    };


    @Override
    public void onClick(View v) {
        stop();
    }

    public void dispose() {
        stop();
    }

    private void stop() {


        if (ringtone != null) {
            ringtone.stop();
        }
        if (mediaPlayer != null) {
            try {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
        // handler.removeCallbacks(stopTask);
        // to do must show main vidget
        mainView.bringUniteCalanderFront();
        Utils.loadAlarms(getContext(), mainView);
        // ((FrameLayout) getParent()).removeView(UnitecAzanVeiw.this);
    }


}