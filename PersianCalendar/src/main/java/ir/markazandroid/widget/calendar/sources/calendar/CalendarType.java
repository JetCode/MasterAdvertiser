package ir.markazandroid.widget.calendar.sources.calendar;

/**
 * Calendars Types
 *
 * @author ebraminio
 */
// So vital, don't ever change names of these
public enum CalendarType {
    SHAMSI, ISLAMIC, GREGORIAN
}
