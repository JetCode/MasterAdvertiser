package ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.byagowi.persiancalendar.R;
import com.byagowi.persiancalendar.databinding.ActivityCalendarBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import ir.markazandroid.widget.calendar.MainView;
import ir.markazandroid.widget.calendar.sources.calendar.CivilDate;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.CalendarUtils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.util.Utils;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.AboutFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.CalendarFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.CompassFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.ConverterFragment;
import ir.markazandroid.widget.calendar.sources.com.byagowi.persiancalendar.view.fragment.PreferenceFragment;
import ir.markazandroid.widget.calendar.sources.com.github.praytimes.Coordinate;

/**
 * Program activity for android
 *
 * @author ebraminio
 */
public class MainActivity extends AppCompatActivity implements /*SharedPreferences.OnSharedPreferenceChangeListener,*/ BottomNavigationView.OnNavigationItemSelectedListener {

    private static final int CALENDAR = 0,
            CONVERTER = 1,
            COMPASS = 2,
            PREFERENCE = 3,
            ABOUT = 4,
            EXIT = 5,
            DEFAULT = CALENDAR; // Default selected fragment
    private final String TAG = MainActivity.class.getName();
    private ActivityCalendarBinding binding;
    private final Class<?>[] fragments = {
            CalendarFragment.class,
            ConverterFragment.class,
            CompassFragment.class,
            PreferenceFragment.class,
            AboutFragment.class
    };
    private int menuPosition = -1; // it should be zero otherwise #selectItem won't be called

    // https://stackoverflow.com/a/3410200
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    // A never used migration
//    private void oneTimeClockDisablingForAndroid5LE() {
//        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
//            String key = "oneTimeClockDisablingForAndroid5LE";
//            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//            if (!prefs.getBoolean(key, false)) {
//                SharedPreferences.Editor edit = prefs.edit();
//                edit.putBoolean(Constants.PREF_WIDGET_CLOCK, false);
//                edit.putBoolean(key, true);
//                edit.apply();
//            }
//        }
//    }

    private static CivilDate creationDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        //setContentView(R.layout.test);
        //FrameLayout o = findViewById(R.id.myyyy);
        //u.init(null);
        MainView a = new MainView(this);
        a.init(null);
        setContentView(a);
//        UIUtils.setTheme(this);
//        Utils.applyAppLanguage(this);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        super.onCreate(savedInstanceState);
//        Utils.initUtils(this);
//        TypeFaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/NotoNaskhArabic-Regular.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
//
//       // Utils.startEitherServiceOrWorker(this);
//
//        // Doesn't matter apparently
//        // oneTimeClockDisablingForAndroid5LE();
//        UpdateUtils.setDeviceCalendarEvents(getApplicationContext());
//        UpdateUtils.update(getApplicationContext(), false);
//
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_calendar);
//        setSupportActionBar(binding.toolbar);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
//                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//
//            binding.toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
//
//        }
//
//        boolean isRTL = UIUtils.isRTL(this);

//        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, binding.drawer, binding.toolbar, R.string.openDrawer, R.string.closeDrawer) {
//            int slidingDirection = isRTL ? -1 : +1;
//
//            @Override
//            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                slidingAnimation(drawerView, slideOffset);
//            }
//
//
//            private void slidingAnimation(View drawerView, float slideOffset) {
//                binding.appMainLayout.setTranslationX(slideOffset * drawerView.getWidth() * slidingDirection);
//                binding.drawer.bringChildToFront(drawerView);
//                binding.drawer.requestLayout();
//            }
//        };

//        binding.drawer.addDrawerListener(drawerToggle);
//        drawerToggle.syncState();
//        String action = getIntent() != null ? getIntent().getAction() : null;
//        if ("COMPASS_SHORTCUT".equals(action)) {
//            selectItem(COMPASS);
//        } else if ("PREFERENCE_SHORTCUT".equals(action)) {
//            selectItem(PREFERENCE);
//        } else if ("CONVERTER_SHORTCUT".equals(action)) {
//            selectItem(CONVERTER);
//        } else if ("ABOUT_SHORTCUT".equals(action)) {
//            selectItem(ABOUT);
//        } else {
//
//            selectItem(DEFAULT);
//        }
        // selectItem(DEFAULT);
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        prefs.registerOnSharedPreferenceChangeListener(this);
//
//        if (Utils.isShowDeviceCalendarEvents()) {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
//                UIUtils.askForCalendarPermission(this);
//            }
//        }

        //   binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
//        AppCompatImageView seasonImage = binding.navigation
//                .getHeaderView(0).findViewById(R.id.season_image);
//        switch (getSeason()) {
//            case "SPRING":
//                seasonImage.setImageResource(R.drawable.spring);
//                break;
//
//            case "SUMMER":
//                seasonImage.setImageResource(R.drawable.summer);
//                break;
//
//            case "FALL":
//                seasonImage.setImageResource(R.drawable.fall);
//                break;
//
//            case "WINTER":
//                seasonImage.setImageResource(R.drawable.winter);
//                break;
//        }

        //  creationDate = CalendarUtils.getGregorianToday();
        //  Utils.applyAppLanguage(this);
    }

    private String getSeason() {
        boolean isSouthernHemisphere = false;
        Coordinate coordinate = Utils.getCoordinate(this);
        if (coordinate != null && coordinate.getLatitude() < 0) {
            isSouthernHemisphere = true;
        }

        int month = CalendarUtils.getPersianToday().getMonth();
        if (isSouthernHemisphere) month = ((month + 6 - 1) % 12) + 1;

        if (month < 4) return "SPRING";
        else if (month < 7) return "SUMMER";
        else if (month < 10) return "FALL";
        else return "WINTER";
    }

    boolean settingHasChanged = false;

//    @Override
//    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//        settingHasChanged = true;
//        if (key.equals(PREF_APP_LANGUAGE)) {
//            boolean persianDigits, changeToAfghanistanHolidays = false;
//            switch (sharedPreferences.getString(PREF_APP_LANGUAGE, DEFAULT_APP_LANGUAGE)) {
//                case LANG_EN_US:
//                    persianDigits = false;
//                    break;
//                case LANG_FA:
//                    persianDigits = true;
//                    break;
//                case LANG_UR:
//                    persianDigits = false;
//                    break;
//                case LANG_FA_AF:
//                    persianDigits = true;
//                    changeToAfghanistanHolidays = true;
//                    break;
//                case LANG_PS:
//                    persianDigits = true;
//                    changeToAfghanistanHolidays = true;
//                    break;
//                default:
//                    persianDigits = true;
//            }
//
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putBoolean(PREF_PERSIAN_DIGITS, persianDigits);
//            // Enable Afghanistan holidays when language changed
//            if (changeToAfghanistanHolidays) {
//                Set<String> currentHolidays =
//                        sharedPreferences.getStringSet(PREF_HOLIDAY_TYPES, new HashSet<>());
//
//                if (currentHolidays.isEmpty() ||
//                        (currentHolidays.size() == 1 && currentHolidays.contains("iran_holidays"))) {
//                    editor.putStringSet(PREF_HOLIDAY_TYPES,
//                            new HashSet<>(Collections.singletonList("afghanistan_holidays")));
//                }
//            }
//            editor.apply();
//        }
//
//        if (key.equals(PREF_SHOW_DEVICE_CALENDAR_EVENTS)) {
//            if (sharedPreferences.getBoolean(PREF_SHOW_DEVICE_CALENDAR_EVENTS, true)) {
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
//                    UIUtils.askForCalendarPermission(this);
//                }
//            }
//        }
//
//        if (key.equals(PREF_APP_LANGUAGE) || key.equals(PREF_THEME)) {
//            restartActivity();
//        }
//
//        if (key.equals(PREF_NOTIFY_DATE)) {
//            if (!sharedPreferences.getBoolean(PREF_NOTIFY_DATE, true)) {
//                stopService(new Intent(this, ApplicationService.class));
//                Utils.startEitherServiceOrWorker(getApplicationContext());
//            }
//        }
//
//        Utils.updateStoredPreference(this);
//        UpdateUtils.update(getApplicationContext(), true);
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == Constants.CALENDAR_READ_PERMISSION_REQUEST_CODE) {
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR)
//                    == PackageManager.PERMISSION_GRANTED) {
//                UIUtils.toggleShowCalendarOnPreference(this, true);
//                if (menuPosition == CALENDAR) {
//                    restartActivity();
//                }
//            } else {
//                UIUtils.toggleShowCalendarOnPreference(this, false);
//            }
//        }
//    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        Utils.initUtils(this);
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
////            binding.drawer.setLayoutDirection(UIUtils.isRTL(this) ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
////        }
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Utils.applyAppLanguage(this);
//        UpdateUtils.update(getApplicationContext(), false);
//        if (!creationDate.equals(CalendarUtils.getGregorianToday())) {
//            restartActivity();
//        }
//    }

    //@Override
    // public void onBackPressed() {
//        if (binding.drawer.isDrawerOpen(GravityCompat.START)) {
//            binding.drawer.closeDrawers();
//        } else if (menuPosition != DEFAULT) {
//            selectItem(DEFAULT);
//        } else {
//            CalendarFragment calendarFragment = (CalendarFragment) getSupportFragmentManager()
//                    .findFragmentByTag(CalendarFragment.class.getName());
//
//            if (calendarFragment != null) {
//                if (calendarFragment.closeSearch()) {
//                    return;
//                }
//            }
//
//            finish();
//        }
    //   }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        // Checking for the "menu" key
//        if (keyCode == KeyEvent.KEYCODE_MENU) {
////            if (binding.drawer.isDrawerOpen(GravityCompat.START)) {
////                binding.drawer.closeDrawers();
////            } else {
////                binding.drawer.openDrawer(GravityCompat.START);
////            }
//            return true;
//        } else {
//            return super.onKeyDown(keyCode, event);
//        }
//    }

//    public void restartActivity() {
//        Intent intent = getIntent();
//        if (menuPosition == CONVERTER)
//            intent.setAction("CONVERTER_SHORTCUT");
//        else if (menuPosition == COMPASS)
//            intent.setAction("COMPASS_SHORTCUT");
//        else if (menuPosition == PREFERENCE)
//            intent.setAction("PREFERENCE_SHORTCUT");
//        else if (menuPosition == ABOUT)
//            intent.setAction("ABOUT_SHORTCUT");
//
//        finish();
//        startActivity(intent);
//    }

//    public void bringPreferences() {
//        selectItem(PREFERENCE);
//    }

    public void selectItem(int item) {
        // onNavigationItemSelected(binding.bottomNavigation.getMenu().getItem(item));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
//        if (menuItem.getItemId() == R.id.exit) {
//            finish();
//            return true;
//        }

        menuItem.setCheckable(true);
        menuItem.setChecked(true);

        int item = -1;
        int itemId = menuItem.getItemId();
        if (itemId == R.id.calendar) {
            item = CALENDAR;
            Log.e(TAG, "    case R.id.calendar:");
        } else if (itemId == R.id.converter) {
            item = CONVERTER;
            Log.e(TAG, "  case R.id.converter:");
        } else if (itemId == R.id.compass) {
            item = COMPASS;
            Log.e(TAG, " case R.id.compass:");
        } else if (itemId == R.id.settings) {
            item = PREFERENCE;
            Log.e(TAG, "  case R.id.settings:");
        } else if (itemId == R.id.about) {
            Log.e(TAG, "   case R.id.about:");
            item = ABOUT;
        }

        if (menuPosition != item) {
            if (settingHasChanged && menuPosition == PREFERENCE) { // update on returning from preferences
                ///Utils.initUtils(this);
                //// UpdateUtils.update(getApplicationContext(), true);
            }

            try {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(
                                R.id.fragment_holder,
                                (Fragment) fragments[item].newInstance(),
                                fragments[item].getName()
                        ).commit();
                menuPosition = item;
            } catch (Exception e) {
                Log.e(TAG, item + " is selected as an index", e);
            }
        }

//        binding.drawer.closeDrawers();
        return true;
    }
}
