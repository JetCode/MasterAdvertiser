package ir.markazandroid.masteradvertiser;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ir.markazandroid.components.signal.Signal;
import ir.markazandroid.components.signal.SignalReceiver;
import ir.markazandroid.masteradvertiser.network.NetworkManager;
import ir.markazandroid.masteradvertiser.service.CampaignService;
import ir.markazandroid.masteradvertiser.service.FetcherService;

public class MainActivity extends AppCompatActivity implements SignalReceiver {

    private NetworkManager networkManager;
    private boolean isBackPressed;
    private View openView1, openView2;

    private int openTries = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView t;

        //doLogin();

        //findViewById(R.id.mainContentHolder).postDelayed(this::start,5000);
        //runOnUiThread(this::start);
        start();

        //findViewById(R.id.mainContentHolder).postDelayed(this::start,3000);

        openView1 = findViewById(R.id.open_view_1);
        openView2 = findViewById(R.id.open_view_2);


        openView1.setOnClickListener(v -> {

            openTries++;
            if (openTries >= 2) {
                openView1.setClickable(false);
                openView2.setOnClickListener(v1 -> onBackPressed());
            }
        });

    }

    private void start(){
        ((MasterAdvertiserApplication)getApplicationContext()).getSignalManager().addReceiver(this);
        getCampaignService().setShowingProperties(findViewById(R.id.parentHolder),getSupportFragmentManager());
        if (!getFetcherService().isStarted())
            getFetcherService().start();
    }

    @Override
    public boolean onSignal(Signal signal) {
        if (signal.getType()==Signal.SIGNAL_ACTIVITY_RECREATE)
            setContentView(R.layout.activity_main);
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((MasterAdvertiserApplication)getApplicationContext()).getSignalManager().removeReceiver(this);
        getCampaignService().stop();
        Log.e("Destroyed","dis");

        if (!isBackPressed)
            throw new RuntimeException();
    }

    @Override
    public void onBackPressed() {
        isBackPressed=true;
        super.onBackPressed();
    }

    private CampaignService getCampaignService(){
        return ((MasterAdvertiserApplication)getApplicationContext()).getCampaignService();
    }
    private FetcherService getFetcherService(){
        return ((MasterAdvertiserApplication)getApplicationContext()).getFetcherService();
    }

    protected NetworkManager getNetworkManager() {
        if (networkManager==null){
            networkManager= new NetworkManager.NetworkManagerBuilder()
                    .from(this)
                    .tag(toString())
                    .build();
        }
        return networkManager;
    }

}
