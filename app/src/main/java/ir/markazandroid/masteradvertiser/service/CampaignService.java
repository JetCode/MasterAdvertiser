package ir.markazandroid.masteradvertiser.service;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import ir.markazandroid.components.signal.Signal;
import ir.markazandroid.components.signal.SignalManager;
import ir.markazandroid.masteradvertiser.MasterAdvertiserApplication;
import ir.markazandroid.masteradvertiser.R;
import ir.markazandroid.masteradvertiser.media.dynamiclayoutinflator.DynamicLayoutInflator;
import ir.markazandroid.masteradvertiser.network.NetworkManager;
import ir.markazandroid.masteradvertiser.object.Campaign;
import ir.markazandroid.masteradvertiser.object.SeriesCampaign;
import ir.markazandroid.masteradvertiser.options.Option;
import ir.markazandroid.masteradvertiser.options.OptionResolver;
import ir.markazandroid.widget.Widget;
import java8.util.stream.StreamSupport;

/**
 * Coded by Ali on 6/22/2019.
 */
public class CampaignService {

    private ViewGroup mainContentHolder;
    private ViewGroup parentHolder;
    private Context context;
    private Handler handler;
    private NetworkManager networkManager;
    private FragmentManager fragmentManager;
    private Campaign campaign;
    private ArrayList<Widget> shouldWaitWidgets;
    private ArrayList<Option> options;


    public CampaignService(Context context) {
        this.context = context;
        networkManager = new NetworkManager.NetworkManagerBuilder()
                .from(context)
                .tag(toString())
                .build();
        handler = new Handler(context.getMainLooper());
        getSignalManager().addReceiver(this::onSignal);
    }

    public void setShowingProperties(ViewGroup parentHolder, FragmentManager fragmentManager) {
        this.parentHolder = parentHolder;
        if (parentHolder != null)
            mainContentHolder = parentHolder.findViewById(R.id.mainContentHolder);
        else mainContentHolder = null;
        this.fragmentManager = fragmentManager;

        if (campaign != null)
            showCampaign(campaign);
    }


    private void showCampaignOnMain(Campaign campaign) {

        this.campaign = campaign;

        doDisposeViews();
        doDisposeOptions();

        Log.e(CampaignService.class.getSimpleName(), String.format("Showing Campaign %s", campaign.getName()));

        if (fragmentManager == null || mainContentHolder == null) {

            Log.e(CampaignService.class.getSimpleName(), String.format("Holder isNull mc:%s fm:%s", mainContentHolder, fragmentManager));
            return;
        }

        ViewGroup toShow = (ViewGroup) DynamicLayoutInflator.inflate(context, campaign.getAndroidData().getXml(), mainContentHolder);
        toShow.setBackgroundColor(Color.WHITE);
        mainContentHolder.removeAllViews();
        mainContentHolder.addView(toShow);

        //options
        options = new ArrayList<>();
        if (campaign.getAndroidData().getOptions() != null) {
            Iterator<String> iterator = campaign.getAndroidData().getOptions().keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                Option option = OptionResolver.resolovOptionByName(context, key);
                if (option != null) {
                    options.add(option);
                    try {
                        option.init(campaign.getAndroidData().getOptions().optJSONObject(key));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        toShow.post(() -> {
            Iterator<String> iterator = campaign.getAndroidData().getExtras().keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                JSONObject extra = campaign.getAndroidData().getExtras().optJSONObject(key);
                Widget widget = (Widget) DynamicLayoutInflator.findViewByIdString(toShow, key);

                widget.setFragmentManager(fragmentManager);

                widget.init(extra);
                if (widget.shouldWaitUntilDone()) {
                    if (shouldWaitWidgets == null)
                        shouldWaitWidgets = new ArrayList<>();

                    widget.setWidgetIsDoneListener(CampaignService.this::widgetSaysItsDone);

                    shouldWaitWidgets.add(widget);
                }
            }

        });
    }

    public void showCampaign(Campaign campaign) {
        handler.post(() -> showCampaignOnMain(campaign));
    }

    private synchronized void widgetSaysItsDone(Widget widget) {
        if (shouldWaitWidgets != null && !shouldWaitWidgets.isEmpty()) {
            shouldWaitWidgets.remove(widget);
            if (shouldWaitWidgets.isEmpty())
                getSeriesService().CSID();
        }
    }

    private synchronized void doDisposeViews() {
        if (mainContentHolder == null)
            return;

        if (mainContentHolder.getChildCount() < 1)
            return;

        ViewGroup main = ((ViewGroup) mainContentHolder.getChildAt(0));

        for (int i = 0; i < main.getChildCount(); i++) {
            View child = main.getChildAt(i);

            if (child instanceof Widget)
                ((Widget) child).dispose();
        }

        mainContentHolder.removeAllViews();

        Log.e(CampaignService.class.getSimpleName(), "Views Disposed");

    }

    private synchronized void doDisposeOptions() {
        if (options == null) return;
        StreamSupport.stream(options)
                .forEach(option -> {
                    try {
                        option.dispose();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

        options = null;
    }

    public synchronized void clearCampaign() {
        if (shouldWaitWidgets != null) {
            shouldWaitWidgets.clear();
            shouldWaitWidgets = null;
        }
        campaign = null;
        handler.post(() -> {
            doDisposeViews();
            doDisposeOptions();
        });
    }


    public void fetchCampaigns(ArrayList<SeriesCampaign> seriesCampaigns, OnCampaignFetchFinishedListener listener) {
        new Thread(() -> {
            for (SeriesCampaign seriesCampaign : seriesCampaigns) {
                try {
                    Campaign campaign = networkManager.getCampaign(seriesCampaign.getCampaignId());
                    if (campaign == null) {
                        listener.failed();
                        return;
                    } else
                        seriesCampaign.setCampaign(campaign);

                } catch (IOException e) {
                    e.printStackTrace();
                    listener.failed();
                    return;
                }
            }
            listener.onFinished(seriesCampaigns);
        }).start();
    }


    private void stopOnMain() {
        if (shouldWaitWidgets != null) {
            shouldWaitWidgets.clear();
            shouldWaitWidgets = null;
        }
        doDisposeViews();
        doDisposeOptions();
        setShowingProperties(null, null);
    }

    public void stop() {
        handler.post(this::stopOnMain);
    }


    private boolean onSignal(Signal signal) {
        switch (signal.getType()) {
            case Signal.SIGNAL_CLEAR_THE_SCREEN:
                clearCampaign();
                return true;

            case Signal.SIGNAL_SCREEN_BLOCK:
                blockScreen();
                break;
            case Signal.SIGNAL_SCREEN_UNBLOCK:
                unBlockScreen();
                break;


        }
        return false;
    }

    private void blockScreen() {
        if (parentHolder != null)
            parentHolder.findViewById(R.id.mirror_view).setVisibility(View.VISIBLE);
    }

    private void unBlockScreen() {
        if (parentHolder != null)
            parentHolder.findViewById(R.id.mirror_view).setVisibility(View.GONE);
    }

    private SeriesService getSeriesService() {
        return ((MasterAdvertiserApplication) context.getApplicationContext()).getSeriesService();
    }


    public interface OnCampaignFetchFinishedListener {
        void onFinished(ArrayList<SeriesCampaign> fetchedSeriesCampaigns);

        void failed();
    }

    private SignalManager getSignalManager() {
        return ((MasterAdvertiserApplication) context.getApplicationContext()).getSignalManager();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        getSignalManager().removeReceiver(this::onSignal);
    }
}