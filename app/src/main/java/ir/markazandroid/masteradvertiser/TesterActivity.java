package ir.markazandroid.masteradvertiser;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import ir.markazandroid.components.ComponentContainer;
import ir.markazandroid.components.model.EFile;
import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.masteradvertiser.object.Campaign;
import ir.markazandroid.masteradvertiser.options.MusicPlayer;
import ir.markazandroid.masteradvertiser.options.Option;
import ir.markazandroid.masteradvertiser.views.playlist.data.Data;
import ir.markazandroid.masteradvertiser.views.playlist.data.DataEntity;
import ir.markazandroid.masteradvertiser.views.playlist.data.Music;
import ir.markazandroid.widget.ScrollTextView;
import ir.markazandroid.widget.calendar.MainView;
import ir.markazandroid.widget.clock.AnalogClock;
import ir.markazandroid.widget.clock.DigitalClock;

public class TesterActivity extends AppCompatActivity {

    Option option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tester);

        Campaign campaign = buildModel();


        // ((MasterAdvertiserApplication) getApplicationContext()).getCampaignService().showCampaign(campaign);

        option = buildOption();


    }

    private Option buildOption() {
        EFile eFile1 = new EFile();
        eFile1.seteFileId("camera.mp3");
        eFile1.setUrl("https://www.soundjay.com/mechanical/camera-focusing-02.mp3");
        Music music1 = new Music();
        music1.seteFile(eFile1);

        EFile eFile2 = new EFile();
        eFile2.seteFileId("polaroid.mp3");
        eFile2.setUrl("http://data.harajgram.ir/polaroid-camera-film-cartridge-load-01.mp3");
        Music music2 = new Music();
        music2.seteFile(eFile2);




        /*EFile eFile2 = new EFile();
        eFile2.seteFileId("musicgeek.ir.mp3");
        eFile2.setUrl("http://dl.musicgeek.ir/soundtrack/film/Christophe%20Beck%20-%20Paperman%20-%20musicgeek.ir.mp3");
        Music music2 = new Music();
        music2.seteFile(eFile2);*/

        ArrayList<DataEntity> musics = new ArrayList<>();
        musics.add(music1);
        musics.add(music2);

        Data data = new Data();
        data.setEntities(musics);

        MusicPlayer.MusicPlayerData musicPlayerData = new MusicPlayer.MusicPlayerData();
        musicPlayerData.setData(data);

        Option musicPlayer = new MusicPlayer(this);
        musicPlayer.init(getParser().get(musicPlayerData));

        return musicPlayer;
    }

    private Campaign buildModel() {
        Campaign campaign = new Campaign();
        campaign.setCampaignId(1);
        campaign.setName("test campaign");
        Campaign.AndroidData androidData = new Campaign.AndroidData();
        androidData.setXml(String.format(Locale.US, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<android.support.constraint.ConstraintLayout xmlns:android=\"http://schemas.android.com/apk/res/android\"\n" +
                        "    xmlns:app=\"http://schemas.android.com/apk/res-auto\"\n" +
                        "    android:layout_width=\"match_parent\"\n" +
                        "    android:layout_height=\"match_parent\">\n" +
                        "\n" +
                        "    <%s\n" +
                        "        android:id=\"@+id/vertical_0_horizontal_0_vertical_100_horizontal_100\"\n" +
                        "        android:layout_width=\"0dp\"\n" +
                        "        android:layout_height=\"0dp\"\n" +
                        "        app:layout_constraintBottom_toBottomOf=\"parent\"\n" +
                        "        app:layout_constraintLeft_toLeftOf=\"parent\"\n" +
                        "        app:layout_constraintRight_toRightOf=\"parent\"\n" +
                        "        app:layout_constraintTop_toTopOf=\"parent\" />\n" +
                        "</android.support.constraint.ConstraintLayout>",

                //this goes the testing view class name
                "ir.markazandroid.widget.calendar.MainView"));

       JSONObject extras = new JSONObject();
//
        AnalogClock.Params params = new AnalogClock.Params();
        params.setTextColor("#ff00ff");
//        params.setSpeed(100);
//        params.setTextColor("#0000ff");
//        //params.setTextColor(Color.BLACK);
//        params.setShouldScroll(true);
//        params.setText("مراسم بزرگداشت سردار بزرگ و پر افتخار اسلام سپهبد شهید حاج قاسم سلیمانی، مجاهد بزرگ شهید ابومهدی المهندس و دیگر شهیدان جنایت تروریستی آمریکا با حضور رهبر معظم انقلاب اسلامی و هزاران نفر از قشرهای مختلف مردم در حسینیه امام خمینی(ره) برگزار شد.\n" +
//                "در این مراسم که رؤسای قوای سه\u200Cگانه، مسئولان لشکری و کشوری، تعدادی از وزیران و مسئولان عراقی، جمعی از نمایندگان گروههای مقاومت و سفرای کشورهای خارجی حضور داشتند، آقای فالح الفیاض رئیس شورای امنیت ملی و حشدالشعبی عراق به نمایندگی از همه مسئولان و مردم این کشور، شهادت سردار بزرگ حاج قاسم سلیمانی را به رهبر انقلاب اسلامی و ملت ایران تسلیت گفت و افزود: برای ما بسیار سنگین است که خون پاک این عزیزان در عراق یعنی در سرزمین حضرت سیدالشهداء علیه\u200Cالسلام به زمین ریخته شد. مردم عراق در غم از دست دادن این شهید دلاور یکپارچه ایستادند و اعلام کردند فرمانده از تو متشکریم.\n" +
//                "آقای فیاض همچنین با گرامیداشت یاد شهید ابومهدی المهندس گفت: شهید ابومهدی همراه و همگام شهید سلیمانی در جنگ مقابل با تکفیر و تأمین امنیت برای مردم عراق بود و سرانجام نیز گوشت و خون آن دو شهید با یکدیگر آمیخته شد.\n" +
//                "در این مراسم همچنین حجت\u200Cالاسلام والمسلمین صدیقی در سخنانی با برشمردن امتیازات برجسته سردار شهید سپهبد سلیمانی، گفت: حاج قاسم عزیز در عین آنکه مجاهد، طراح، هوشمند، فرمانده و فاتح بود، با الگوگیری از حضرت صدیقه کبری سلام\u200Cالله\u200Cعلیها، نسبت به ولایت کاملاً مطیع و فرمانبردار بود و شهادت او نیز مانند حیاتش، موجب عزت اسلام و انقلاب شد.\n" +
//                "در این مراسم، قاریان قرآن کریم به تلاوت آیاتی از کلام\u200Cالله مجید پرداختند و ذاکران اهل\u200Cبیت علیهم\u200Cالسلام در مصائب حضرت فاطمه\u200Cزهرا سلام\u200Cالله\u200Cعلیها نوحه\u200Cسرایی کردند و اشعاری در فضلیت جهاد و مجاهدان راه خدا از جمله شهید سلیمانی قرائت کردند.");
//
//       params.setText(" derert ertert erter ert ert erte ert ert ert er ert ertert ree DGEER WER WEW E WE RWE RWER WER WTTYJ HTYJTYJ TJ TJ ");
//        EFile eFile = new EFile();
//        eFile.seteFileId("bellada_personal_license.ttf");
//        eFile.setUrl("http://data.harajgram.ir/bellada%20personal%20license.ttf");
//
//       params.setFont(eFile);
//
//
       //JSONObject viewExtras = getParser().get(params);
        JSONObject viewExtras = new JSONObject();
//        //here u can customize the view
        try {
//            EFile bg = new EFile();
//            bg.seteFileId("omgbg");
//            bg.setUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Ali_Khamenei_crop.jpg/1200px-Ali_Khamenei_crop.jpg");
//            viewExtras.putOpt("bgImage", getParser().get(bg));
//            //viewExtras.putOpt("bgColor",Color.YELLOW);
//
//
            extras.put("vertical_0_horizontal_0_vertical_100_horizontal_100", viewExtras);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        androidData.setExtras(extras);
        campaign.setAndroidData(androidData);

        return campaign;
    }

    public Parser getParser() {
        return ((ComponentContainer) getApplication()).getParser();
    }
}
