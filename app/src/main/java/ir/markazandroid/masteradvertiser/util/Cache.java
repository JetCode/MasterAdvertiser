package ir.markazandroid.masteradvertiser.util;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.masteradvertiser.object.Schedule;

/**
 * Coded by Ali on 9/26/2019.
 */
public class Cache {

    private final static String SCHEDULE_FILE_PATH="schedule";

    private Context context;
    private Parser parser;

    public Cache(Context context, Parser parser) {
        this.context = context;
        this.parser = parser;
    }

    public void cacheSchedule(Schedule schedule){
        try {
            FileOutputStream outputStream =context.openFileOutput(SCHEDULE_FILE_PATH,Context.MODE_PRIVATE);
            outputStream.write(parser.get(schedule).toString().getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Schedule getCachedSchedule(){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.openFileInput(SCHEDULE_FILE_PATH)));
            String s;
            StringBuilder builder = new StringBuilder();
            while ((s=reader.readLine())!=null){
                builder.append(s);
            }
            JSONObject o  =new JSONObject(builder.toString());
            //Log.i(getClass().getSimpleName(),o.toString(1));
            return parser.get(Schedule.class,o);
        } catch (IOException | JSONException e) {
            return null;
        }
    }
}
