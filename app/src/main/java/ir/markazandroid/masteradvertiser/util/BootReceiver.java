package ir.markazandroid.masteradvertiser.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import ir.markazandroid.masteradvertiser.MainActivity;
import ir.markazandroid.masteradvertiser.MasterAdvertiserApplication;

/**
 * Created by Ali on 23/01/2017.
 */
public class BootReceiver extends BroadcastReceiver {
    public static final int Starter_activity = 1254786;

    @Override
    public void onReceive(Context context, Intent arg) {
        Log.e("master","run");
        Intent in = new Intent(context, MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(in);

        (((MasterAdvertiserApplication)context.getApplicationContext())).getPreferencesManager().setSystemBootCompleted(true);
    }

}
