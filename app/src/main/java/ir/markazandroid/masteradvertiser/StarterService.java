package ir.markazandroid.masteradvertiser;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class StarterService extends Service {
    public StarterService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timer timer = new Timer();
        Log.e("master","start command");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Log.e("master","run");
                Intent in = new Intent(getApplicationContext(),MainActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
            }
        },20000);

        return START_NOT_STICKY;
    }
}
