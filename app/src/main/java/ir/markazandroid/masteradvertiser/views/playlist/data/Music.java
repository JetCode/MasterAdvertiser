package ir.markazandroid.masteradvertiser.views.playlist.data;

import ir.markazandroid.components.network.JSONParser.annotations.JSON;

public class Music extends DataEntity {
    public static final String DATA_TYPE_MUSIC = "music";

    private int volume;

    public Music() {
        setDataType(DATA_TYPE_MUSIC);
    }

    @JSON
    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}