package ir.markazandroid.masteradvertiser.views.camera;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import org.json.JSONObject;

import ir.markazandroid.masteradvertiser.R;
import ir.markazandroid.widget.Widget;


public class CameraWidget extends FrameLayout implements Widget {

    private static final String TAG = "GoldCameraActivity";

    private Camera mCamera;
    private CameraPreview mPreview;

    public CameraWidget(@NonNull Context context) {
        super(context);
        View mainView = LayoutInflater.from(getContext()).inflate(R.layout.activity_gold_camera, this, false);
        addView(mainView);
    }

    @Override
    public void init(JSONObject extras) {

        // Create an instance of Camera
        mCamera = getCameraInstance(extras.optInt("cameraId", 0));
        //mCamera.startFaceDetection();
        // mCamera.setDisplayOrientation(270);

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(getContext(), mCamera);
        FrameLayout preview = findViewById(R.id.camera_preview);
        preview.addView(mPreview);
    }


    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware() {
        // this device has a camera
// no camera on this device
        return getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            c = Camera.open(cameraId);// attempt to get a Camera instance
            if (c == null) c = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public static Camera openFrongCamera() {
        int numberOfCameras = Camera.getNumberOfCameras();
        Log.e(TAG, "openFrongCamera: " + numberOfCameras);
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        for (int i = 0; i < numberOfCameras; i++) {
            Log.e(TAG, "openFrongCamera: " + i);
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return Camera.open(i);
            }
        }
        return null;
    }


    @Override
    public void dispose() {
        mCamera.release();
    }
}
