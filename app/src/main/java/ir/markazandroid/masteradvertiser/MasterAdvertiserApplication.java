package ir.markazandroid.masteradvertiser;

import android.app.Application;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.File;

import ir.markazandroid.components.ComponentContainer;
import ir.markazandroid.components.aidl.PoliceBridge;
import ir.markazandroid.components.aidl.PoliceHandlerHelper;
import ir.markazandroid.components.db.DataBase;
import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.components.network.NetworkClient;
import ir.markazandroid.components.network.downloader.MasterDownloader;
import ir.markazandroid.components.signal.Signal;
import ir.markazandroid.components.signal.SignalManager;
import ir.markazandroid.components.signal.SignalReceiver;
import ir.markazandroid.masteradvertiser.service.CampaignService;
import ir.markazandroid.masteradvertiser.service.FetcherService;
import ir.markazandroid.masteradvertiser.service.ScheduleService;
import ir.markazandroid.masteradvertiser.service.SeriesService;
import ir.markazandroid.masteradvertiser.service.TimelineService;
import ir.markazandroid.masteradvertiser.util.Cache;
import ir.markazandroid.masteradvertiser.util.PreferencesManager;

/**
 * Coded by Ali on 6/19/2019.
 */
public class MasterAdvertiserApplication extends Application implements SignalReceiver, ComponentContainer {

    public static final String SHARED_PREFRENCES = "shared_pref";

    private NetworkClient networkClient;
    private SignalManager signalManager;
    private Parser parser;
    private PreferencesManager preferencesManager;
    private DataBase dataBase;
    private PoliceBridge policeBridge;
    private boolean isInternetConnected = false;
    private MasterDownloader masterDownloader;

    private CampaignService campaignService;
    private FetcherService fetcherService;
    private ScheduleService scheduleService;
    private TimelineService timelineService;
    private SeriesService seriesService;

    private Cache cache;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e(getClass().getSimpleName(),"Start OF This App");

        //getPoliceBridge();
        //installApp();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/advertiser/image");
        myDir.mkdirs();
        Picasso picasso =new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(myDir,200_000_000))
                .build();
        Picasso.setSingletonInstance(picasso);
        getSignalManager().addReceiver(this);

     //   Intent intent = new Intent(this,StarterService.class);
     //   startService(intent);


        Intent in = new Intent(this, MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);

        getFetcherService().start();

      //  if (getPreferencesManager().getSystemBootCompleted()){
      //
       // }

    }

    public Parser getParser() {
        if (parser==null) {
            parser = new Parser();
        }
        return parser;
    }

    public DataBase getDataBase(){
        if (dataBase==null) dataBase=new DataBase(this);
        return dataBase;
    }

    @Override
    public boolean onSignal(Signal signal) {
        if (signal.getType() == Signal.SIGNAL_LOGIN) {
            // setUser((User) signal.getExtras());
            Log.e(MasterAdvertiserApplication.this.toString(), "login signal received " /*+ user.getUsername()*/);
            return true;
        } else if (signal.getType() == Signal.SIGNAL_LOGOUT) {
            Log.e(MasterAdvertiserApplication.this.toString(), "logout signal received ");
            //getNetworkClient().deleteCookies();
            //DeleteToken deleteToken = new DeleteToken();
            //deleteToken.execute();
            //setUser(null);
            return true;
        }
        else if (signal.getType()==Signal.START_MAIN_ACTIVITY){
           /* if (getFrontActivity()==null) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }*/
        }
        else if (signal.getType()==Signal.OPEN_SOCKET_HEADER_RECEIVED){
        }else if (signal.getType()==Signal.DOWNLOADER_NO_NETWORK){
            isInternetConnected=false;
        }else if (signal.getType()==Signal.DOWNLOADER_NETWORK){
            isInternetConnected=true;
        }
        return false;
    }

    public PreferencesManager getPreferencesManager() {
        if (preferencesManager==null) preferencesManager=new PreferencesManager(getSharedPreferences(SHARED_PREFRENCES,MODE_PRIVATE));
        return preferencesManager;
    }

    public NetworkClient getNetworkClient(){
        if (networkClient==null){
            networkClient=new NetworkClient(this);
        }
        return networkClient;
    }

    public SignalManager getSignalManager() {
        if (signalManager == null) signalManager = new SignalManager(this);
        return signalManager;
    }

    public boolean isInternetConnected() {
        return isInternetConnected;
    }

    @Override
    public PoliceBridge getPoliceBridge() {
        if (policeBridge==null){
            policeBridge=new PoliceHandlerHelper(this,getSignalManager());
        }
        return policeBridge;
    }

    public MasterDownloader getMasterDownloader() {
        if (masterDownloader==null){
            masterDownloader=new MasterDownloader(this);
            masterDownloader.start();
        }
        return masterDownloader;
    }

    public CampaignService getCampaignService() {
        if (campaignService==null) campaignService=new CampaignService(this);
        return campaignService;
    }

    public FetcherService getFetcherService() {
        if (fetcherService==null) fetcherService=new FetcherService(this);
        return fetcherService;
    }

    public ScheduleService getScheduleService() {
        if (scheduleService==null) scheduleService=new ScheduleService(this);
        return scheduleService;
    }

    public TimelineService getTimelineService() {
        if (timelineService==null) timelineService=new TimelineService(this);
        return timelineService;
    }

    public SeriesService getSeriesService() {
        if (seriesService==null) seriesService=new SeriesService(this);
        return seriesService;
    }

    public Cache getCache() {
        if (cache==null) cache=new Cache(this,getParser());
        return cache;
    }
}
