package ir.markazandroid.masteradvertiser.options;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ir.markazandroid.components.network.JSONParser.Parser;
import ir.markazandroid.components.network.JSONParser.annotations.JSON;
import ir.markazandroid.components.network.downloader.MasterDownloader;
import ir.markazandroid.components.signal.SignalManager;
import ir.markazandroid.masteradvertiser.MasterAdvertiserApplication;
import ir.markazandroid.masteradvertiser.views.playlist.data.Data;
import ir.markazandroid.masteradvertiser.views.playlist.data.Music;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

public class MusicPlayer implements Option {

    public static final String OPTION_NAME = "music_player";

    private final Parser parser;
    private final Context context;
    private MusicPlayerData data;
    private final MasterDownloader masterDownloader;
    private final SignalManager signalManager;
    private final List<Music> musics;
    private SimpleExoPlayer soundPlayer;
    private int soundIndex = 0;
    private Thread playerThread;
    private final Object lockObject = new Object();
    private boolean isPlaying = false;


    public MusicPlayer(Context context) {
        this.context = context;
        MasterAdvertiserApplication masterAdvertiserApplication = (MasterAdvertiserApplication) context.getApplicationContext();
        parser = masterAdvertiserApplication.getParser();
        masterDownloader = masterAdvertiserApplication.getMasterDownloader();
        signalManager = masterAdvertiserApplication.getSignalManager();
        musics = Collections.synchronizedList(new ArrayList<>());

        playerThread = new Thread(this::startPlay);
    }

    @Override
    public void init(JSONObject jData) {
        this.data = parser.get(MusicPlayerData.class, jData);
        musics.addAll(StreamSupport.stream(data.getData().getEntities())
                .filter(dataEntity -> dataEntity instanceof Music)
                .map(dataEntity -> (Music) dataEntity)
                .peek(music ->
                        masterDownloader.giveMeFile(music.geteFile(), MusicPlayer.this,
                                (MasterDownloader.EFileCacheFetchListener) music::setFile
                                , (MasterDownloader.EFileNetFetchListener) (file, cached) -> music.setFile(file)))
                .collect(Collectors.toList()));

        if (!musics.isEmpty()) {
            initSoundPlayer();
            playerThread.start();
        }

        Log.i(OPTION_NAME, "music player init");
    }

    @Override
    public void dispose() {
        StreamSupport.stream(musics)
                .forEach(music -> masterDownloader.removeListenerByTag(music.geteFile(), MusicPlayer.this));

        playerThread.interrupt();

        synchronized (lockObject) {
            soundPlayer.release();
        }

        Log.i(OPTION_NAME, "music player disposed");
    }


    //Called in async thread
    private void startPlay() {
        while (true) {
            synchronized (lockObject) {
                try {
                    Music chosenMusic = musics.get(soundIndex);

                    if (chosenMusic.getFile() == null) {
                        Log.i(OPTION_NAME, "File for index " + soundIndex + " not downloaded yet. recheck in 1 sec");
                        Thread.sleep(1000);
                    } else {
                        Log.d(OPTION_NAME, "File for index " + soundIndex + " ready to play");
                        if (!isPlaying && (soundPlayer.getPlaybackState() == Player.STATE_IDLE
                                || soundPlayer.getPlaybackState() == Player.STATE_ENDED)) {
                            Log.d(OPTION_NAME, "File for index " + soundIndex + " started to play");
                            soundPlayer.prepare(newVideoSource(chosenMusic.getFile()));
                            isPlaying = true;
                            if (soundIndex >= musics.size() - 1)
                                soundIndex = 0;
                            else
                                soundIndex++;
                        }

                        Log.d(OPTION_NAME, "waiting on player signal");
                        lockObject.wait();
                    }
                } catch (InterruptedException ignored) {
                    Log.d(OPTION_NAME, "interrupted");
                    return;
                }
            }
        }
    }


    private void initSoundPlayer() {

        soundPlayer = newSimpleExoPlayer();
        soundPlayer.setPlayWhenReady(true);

        soundPlayer.addListener(new Player.DefaultEventListener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Log.i(OPTION_NAME, "player state update: " + playbackState);
                if (soundPlayer.getPlaybackState() == Player.STATE_ENDED) {

                    Log.d(OPTION_NAME, "waiting for sync (onPlayerStateChanged)");
                    synchronized (lockObject) {
                        Log.d(OPTION_NAME, "sending ready for next file signal");
                        isPlaying = false;
                        lockObject.notify();
                    }
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Log.e(OPTION_NAME, "player error");
                error.printStackTrace();
                Log.d(OPTION_NAME, "waiting for sync (onPlayerError)");
                synchronized (lockObject) {
                    Log.d(OPTION_NAME, "recreating the player");
                    soundPlayer.release();
                    initSoundPlayer();
                    isPlaying = false;
                }
            }
        });
    }

    public void pause() {
        if (soundPlayer != null)
            soundPlayer.setPlayWhenReady(false);
    }

    public void resume() {
        if (soundPlayer != null)
            soundPlayer.setPlayWhenReady(true);
    }

    private SimpleExoPlayer newSimpleExoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        return ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);
    }

    private MediaSource newVideoSource(File file) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        String userAgent = Util.getUserAgent(context, OPTION_NAME);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, userAgent, bandwidthMeter);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        return new ExtractorMediaSource(Uri.fromFile(file), dataSourceFactory, extractorsFactory, null, null);
    }

    public static class MusicPlayerData implements Serializable {
        private Data data;

        @JSON(classType = JSON.CLASS_TYPE_OBJECT, clazz = Data.class)
        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

    }


}
