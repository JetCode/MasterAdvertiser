package ir.markazandroid.masteradvertiser.options;

import android.content.Context;

public class OptionResolver {

    public static Option resolovOptionByName(Context context, String key) {
        switch (key) {
            case MusicPlayer.OPTION_NAME:
                return new MusicPlayer(context);
            default:
                return null;
        }
    }
}
