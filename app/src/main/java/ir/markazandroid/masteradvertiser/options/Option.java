package ir.markazandroid.masteradvertiser.options;

import org.json.JSONObject;

public interface Option {

    void init(JSONObject jsonObject);

    void dispose();
}
